<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Auth;

class MentorAuth {
    protected $auth;

    function __construct(Guard $auth) {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next){
        if ($this->auth->guest())
            return redirect()->action('FrontPageController@auth')->with('message', 'Введите логин и пароль для доступа');

        $roles = Auth::user()->roles()->get();
        $role = $roles[0]->pivot->role_id;
        if($role!=3)
        	return redirect()->action('FrontPageController@auth')->with('message', 'Вы не можете иметь доступ к роли ментора');
        return $next($request);
    }
}
