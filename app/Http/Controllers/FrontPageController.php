<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\Client;
use App\Mentor;
use App\Location;
use App\Job;
use Illuminate\Http\Request;
use Hash;
use Auth;

class FrontPageController extends Controller
{

    public function oferta(){

        return view("front.oferta.oferta");
    }

    public function oferta_kz(){

        return view("front.oferta.oferta_kz");
    }

    public function index (){

        $ar['jobs'] = Job::limit(15)->get();
        $ar['mentors'] = Mentor::where('approved', 1)->limit(36)->orderBy('id', 'desc')->paginate(12);
        $ar['locations'] = Location::all();
        $ar['proffessions'] = Job::all();

    	return view('front.index.index', $ar);
    }

    public function searchPage (Request $r){

        if($r->has('price') && $r->has('cities') && $r->has('job')){
            $ar['results'] = Mentor::where('approved', 1)
                                    ->where("price", "<=", $r->input('price'))
                                    ->where("location_id", $r->input('cities'))
                                    ->paginate(4);
            if($r->input('cities') == 0){
                $ar['results'] = Mentor::where('approved', 1)
                                    ->where("price", "<=", $r->input('price'))
                                    ->paginate(4);
            }
        }else{
            $ar['results'] = Mentor::where('approved', 1)->limit(30)->paginate(4);
        }

        $ar['query'] = $r->input('query');
        $ar['price'] = $r->input('price');
        $ar['city'] = $r->input('cities');
        $ar['job'] = $r->input('job');

        $ar['locations'] = Location::all();
        $ar['proffessions'] = Job::all();

        return view('front.search_result.search_result', $ar);
    }

    public function mentorPage ($id){

        $mentor = Mentor::where('id', $id)->get();
        $ar['mentor'] = $mentor[0];
        if($ar['mentor']->approved == 0){
            return "Такого ментора нет";
        }else{
            return view('front.profile_mentor.public_profile', $ar);
        }
    }

    public function auth (){
    	if(Auth::user()){
    		$roles = Auth::user()->roles()->get();
    		$role = $roles[0]->pivot->role_id;
    		switch ($role) {
    			//User
    			case 2:
    				return redirect()->action('User\UserPageController@getProfile');
    				break;
    			//Mentor
    			case 3:
    				return redirect()->action('Mentor\MentorPageController@getProfile');
    				break;
    			case 1:
    				return redirect('/home');
    				break;
    			default:
    				return redirect()->action('FrontPageController@auth')->with('message', 'Введите логин и пароль для доступа');
    				break;
    		}
    	}else{
    		$ar['action'] = action("FrontPageController@POSTauth");
    		return view('front.auth.auth', $ar);
    	}
    }

    public function POSTauth (Request $r){

    	$email = $r->input("email");
    	$password = $r->input("password");

    	$user = User::where("email", $email)
    				->where("password", Hash::make($password))
    				->get();

      	$credentials = array('email' => $email , 'password' => $password);
      	if(Auth::attempt($credentials)){
      		return redirect()->back();
      	}else{
      		return redirect()->back()->with('message', 'Таких данных не найдено');  
      	}
    }

    public function logout (){

    	Auth::logout();
    	return redirect()->action('FrontPageController@auth')->with('message', 'Вы успешно вышли из системы');
    }

    public function register (){

    	$ar["action"] = action("FrontPageController@POSTregister");

    	return view('front.auth.register', $ar);
    }

    public function POSTregister (Request $r){

    	$check = User::where("email", $r->input('email'))
    					->get();
    	if($check->count() > 0){
    		return back()->with('message', 'Пользователь с таким email уже зарегистрирован.'); 
    	}

    	$user = User::create([
            'name' => $r->input('name'),
            'surname' => $r->input('surname'),
            'email' => $r->input('email'),
            'approved' => 1,
            'password' => Hash::make($r->input('password')),
        ]);
        $user->roles()->sync([$r->input('user_type')]);

        $role = $r->input('user_type');

        if($role == 2){
        	Client::create([
        		'user_id' => $user->id,
                'location' => "Не указано",
                'education' => "Не указано",
                'age' => "Не указано",
                'phone' => "Не указано",
                'selected_job' => "Не указано",
        	]);
        }
        if($role == 3){
        	Mentor::create([
        		'user_id' => $user->id,
                'approved' => 0,
                'about' => "Укажите необходимую информацию о себе. Заполните профиль.",
                'speciality' => "Не указано",
                'job_place' => "Не указано",
                'job_experience' => "Не указано",
                'education' => "Не указано",
                'price' => "0",
                'contacts' => "Не указано",
                'useful_1' => "Не указано",
                'useful_2' => "Не указано",
                'useful_3' => "Не указано",
                'location' => "Не указано",
        	]);
        }

    	return redirect("/auth")->with('message', 'Вы успешно зарегистрированы. Можете авторизоваться с указанными данными.'); 
    }

    public function jobPage ($id){

        $ar['job'] = Job::where("id", $id)->with("jobMentors")->get();
        $ar['results'] = $ar['job'][0]->jobMentors;

        return view('front.proffession.proffession', $ar);
    }

}












