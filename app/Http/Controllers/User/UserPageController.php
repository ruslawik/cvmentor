<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Client;
use App\User;
use Auth;

class UserPageController extends Controller
{
    
    public function getProfile (){

		$client = Client::where("user_id", Auth::user()->id)->get();

		$users = User::all()->pluck('email', 'id')->prepend(trans('global.pleaseSelect'), '');

        $client = $client[0]->load('user');

        return view('front.profile_user.profile_user', compact('users', 'client'));
	}

	public function profileUpdatePost (Request $request){

		$client = Client::where("user_id", Auth::user()->id)->get();

		$users = User::all()->pluck('email', 'id')->prepend(trans('global.pleaseSelect'), '');

        $client = $client[0]->load('user');

		$data = request()->except(['_token', 'photo', 'client_name', 'client_surname']);

		$client->where("user_id", Auth::user()->id)
				->update($data);

		User::where("id", Auth::user()->id)->update(["name" => $request->input('client_name'), "surname" => $request->input('client_surname')]);

        if ($request->input('photo', false)) {
            if (!$client->photo || $request->input('photo') !== $client->photo->file_name) {
                $client->addMedia(storage_path('tmp/uploads/' . $request->input('photo')))->toMediaCollection('photo');
            }
        } elseif ($client->photo) {
            $client->photo->delete();
        }

		return back();
	}

}
