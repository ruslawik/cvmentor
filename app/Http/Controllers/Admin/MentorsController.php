<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyMentorRequest;
use App\Http\Requests\StoreMentorRequest;
use App\Http\Requests\UpdateMentorRequest;
use App\Job;
use App\Location;
use App\Mentor;
use App\User;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class MentorsController extends Controller
{
    use MediaUploadingTrait;

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $query = Mentor::with(['user', 'jobs', 'location'])->select(sprintf('%s.*', (new Mentor)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'mentor_show';
                $editGate      = 'mentor_edit';
                $deleteGate    = 'mentor_delete';
                $crudRoutePart = 'mentors';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->editColumn('approved', function ($row) {
                return $row->approved ? "YES" : "NO";
            });
            $table->addColumn('user_email', function ($row) {
                return $row->user ? $row->user->email : '';
            });

            $table->editColumn('photo', function ($row) {
                if ($photo = $row->photo) {
                    return sprintf(
                        '<a href="%s" target="_blank"><img src="%s" width="50px" height="50px"></a>',
                        $photo->url,
                        $photo->thumbnail
                    );
                }

                return '';
            });
            $table->editColumn('job', function ($row) {
                $labels = [];

                foreach ($row->jobs as $job) {
                    $labels[] = sprintf('<span class="label label-info label-many">%s</span>', $job->name);
                }

                return implode(' ', $labels);
            });
            $table->editColumn('job_place', function ($row) {
                return $row->job_place ? $row->job_place : "";
            });
            $table->editColumn('education', function ($row) {
                return $row->education ? $row->education : "";
            });
            $table->editColumn('price', function ($row) {
                return $row->price ? $row->price : "";
            });
            $table->addColumn('location_name', function ($row) {
                return $row->location ? $row->location->name : '';
            });

            $table->editColumn('age', function ($row) {
                return $row->age ? $row->age : "";
            });

            $table->rawColumns(['actions', 'placeholder', 'user', 'photo', 'job', 'location']);

            return $table->make(true);
        }

        return view('admin.mentors.index');
    }

    public function create()
    {
        abort_if(Gate::denies('mentor_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $users = User::all()->pluck('email', 'id')->prepend(trans('global.pleaseSelect'), '');

        $jobs = Job::all()->pluck('name', 'id');

        $locations = Location::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.mentors.create', compact('users', 'jobs', 'locations'));
    }

    public function store(StoreMentorRequest $request)
    {
        $mentor = Mentor::create($request->all());
        $mentor->jobs()->sync($request->input('jobs', []));

        if ($request->input('photo', false)) {
            $mentor->addMedia(storage_path('tmp/uploads/' . $request->input('photo')))->toMediaCollection('photo');
        }

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $mentor->id]);
        }

        return redirect()->route('admin.mentors.index');
    }

    public function edit(Mentor $mentor)
    {
        abort_if(Gate::denies('mentor_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $users = User::all()->pluck('email', 'id')->prepend(trans('global.pleaseSelect'), '');

        $jobs = Job::all()->pluck('name', 'id');

        $locations = Location::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $mentor->load('user', 'jobs', 'location');

        return view('admin.mentors.edit', compact('users', 'jobs', 'locations', 'mentor'));
    }

    public function update(UpdateMentorRequest $request, Mentor $mentor)
    {
        $mentor->update($request->all());
        $mentor->jobs()->sync($request->input('jobs', []));

        if ($request->input('photo', false)) {
            if (!$mentor->photo || $request->input('photo') !== $mentor->photo->file_name) {
                $mentor->addMedia(storage_path('tmp/uploads/' . $request->input('photo')))->toMediaCollection('photo');
            }
        } elseif ($mentor->photo) {
            $mentor->photo->delete();
        }

        return redirect()->route('admin.mentors.index');
    }

    public function show(Mentor $mentor)
    {
        abort_if(Gate::denies('mentor_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $mentor->load('user', 'jobs', 'location');

        return view('admin.mentors.show', compact('mentor'));
    }

    public function destroy(Mentor $mentor)
    {
        abort_if(Gate::denies('mentor_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $mentor->delete();

        return back();
    }

    public function massDestroy(MassDestroyMentorRequest $request)
    {
        Mentor::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('mentor_create') && Gate::denies('mentor_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new Mentor();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media', 'public');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
