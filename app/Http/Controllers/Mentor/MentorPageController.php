<?php

namespace App\Http\Controllers\Mentor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Mentor;
use App\Job;
use App\Location;
use Auth;


class MentorPageController extends Controller
{
 	
	public function getProfile (){

		$mentor = Mentor::where("user_id", Auth::user()->id)->get();

		$users = User::all()->pluck('email', 'id')->prepend(trans('global.pleaseSelect'), '');

        $jobs = Job::all()->pluck('name', 'id');

        $locations = Location::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $mentor = $mentor[0]->load('user', 'jobs', 'location');

        return view('front.profile_mentor.profile_mentor', compact('users', 'jobs', 'locations', 'mentor'));
	}

	public function profileUpdatePost (Request $request){

		$mentor = Mentor::where("user_id", Auth::user()->id)->get();

		$users = User::all()->pluck('email', 'id')->prepend(trans('global.pleaseSelect'), '');

        $jobs = Job::all()->pluck('name', 'id');

        $locations = Location::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $mentor = $mentor[0]->load('user', 'jobs', 'location');

		$data = request()->except(['_token', 'jobs', 'photo', 'mentor_name', 'mentor_surname']);

		$mentor->where("user_id", Auth::user()->id)
				->update($data);
        $mentor->jobs()->sync($request->input('jobs', []));

        User::where("id", Auth::user()->id)->update(["name" => $request->input('mentor_name'), "surname" => $request->input('mentor_surname')]);

        if ($request->input('photo', false)) {
            if (!$mentor->photo || $request->input('photo') !== $mentor->photo->file_name) {
                $data = getimagesize(storage_path('tmp/uploads/' . $request->input('photo')));
                $width = $data[0];
                $height = $data[1];
                if($width != $height){
                    return redirect()->back()->with('message', 'Ошибка: загрузите ровный квадрат для аватара (ширина фото = высота фото)');
                }
                $mentor->addMedia(storage_path('tmp/uploads/' . $request->input('photo')))->toMediaCollection('photo');
            }
        } elseif ($mentor->photo) {
            $mentor->photo->delete();
        }

		return back();
	}

}
