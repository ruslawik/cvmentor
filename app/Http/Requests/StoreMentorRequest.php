<?php

namespace App\Http\Requests;

use App\Mentor;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreMentorRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('mentor_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'user_id'        => [
                'required',
                'integer',
            ],
        ];
    }
}
