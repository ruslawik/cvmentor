<?php

namespace App\Http\Requests;

use App\Job;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreJobRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('job_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'name'      => [
                'required',
            ],
            'about'     => [
                'required',
            ],
            'univer_1'  => [
                'required',
            ],
            'univer_2'  => [
                'required',
            ],
            'univer_3'  => [
                'required',
            ],
            'sub_job_1' => [
                'required',
            ],
            'sub_job_2' => [
                'required',
            ],
            'sub_job_3' => [
                'required',
            ],
            'sub_job_4' => [
                'required',
            ],
            'sub_job_5' => [
                'required',
            ],
        ];
    }
}
