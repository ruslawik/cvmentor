<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Client extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    public $table = 'clients';

    protected $appends = [
        'photo',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public static $searchable = [
        'age',
        'photo',
        'phone',
        'money',
        'awards',
        'location',
        'weekness',
        'education',
        'interests',
        'volunteer',
        'kabiletter',
        'strongness',
        'selected_job',
        'certificates',
        'additional_info',
    ];

    protected $fillable = [
        'age',
        'phone',
        'money',
        'awards',
        'user_id',
        'location',
        'weekness',
        'volunteer',
        'interests',
        'education',
        'kabiletter',
        'strongness',
        'created_at',
        'updated_at',
        'deleted_at',
        'certificates',
        'selected_job',
        'additional_info',
    ];

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getPhotoAttribute()
    {
        $file = $this->getMedia('photo')->last();

        if ($file) {
            $file->url       = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
        }

        return $file;
    }
}
