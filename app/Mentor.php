<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Mentor extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    public $table = 'mentors';

    protected $appends = [
        'photo',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public static $searchable = [
        'age',
        'approved',
        'price',
        'about',
        'rating',
        'date_1',
        'online',
        'date_5',
        'date_4',
        'date_3',
        'date_2',
        'offline',
        'useful_3',
        'useful_2',
        'contacts',
        'useful_1',
        'education',
        'job_place',
        'speciality',
        'job_experience',
    ];

    protected $fillable = [
        'age',
        'approved',
        'price',
        'about',
        'rating',
        'online',
        'date_5',
        'date_4',
        'date_3',
        'date_2',
        'date_1',
        'user_id',
        'offline',
        'useful_2',
        'useful_3',
        'useful_1',
        'contacts',
        'education',
        'job_place',
        'speciality',
        'created_at',
        'updated_at',
        'deleted_at',
        'location_id',
        'job_experience',
    ];

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getPhotoAttribute()
    {
        $file = $this->getMedia('photo')->last();

        if ($file) {
            $file->url       = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
        }

        return $file;
    }

    public function jobs()
    {
        return $this->belongsToMany(Job::class);
    }

    public function location()
    {
        return $this->belongsTo(Location::class, 'location_id');
    }
}
