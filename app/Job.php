<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Job extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    public $table = 'jobs';

    protected $appends = [
        'photo',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public static $searchable = [
        'name',
        'about',
        'univer_1',
        'univer_2',
        'univer_3',
        'univer_4',
        'univer_5',
        'sub_job_1',
        'sub_job_2',
        'sub_job_3',
        'sub_job_4',
        'sub_job_5',
    ];

    protected $fillable = [
        'name',
        'about',
        'univer_1',
        'univer_2',
        'univer_3',
        'univer_4',
        'univer_5',
        'sub_job_1',
        'sub_job_2',
        'sub_job_3',
        'sub_job_4',
        'sub_job_5',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);
    }

    public function getPhotoAttribute()
    {
        $file = $this->getMedia('photo')->last();

        if ($file) {
            $file->url       = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
        }

        return $file;
    }

    public function jobMentors()
    {
        return $this->belongsToMany(Mentor::class);
    }

}
