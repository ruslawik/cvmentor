<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $users = [
            [
                'id'             => 1,
                'name'           => 'Admin',
                'email'          => 'admin@admin.com',
                'password'       => '$2y$10$DNC0CLnGf1S7mCdfw1UvDue6TdR9cAwelDSB9PPACygrVYm4RGLMC',
                'remember_token' => null,
                'approved'       => 1,
                'surname'        => '',
            ],
        ];

        User::insert($users);
    }
}
