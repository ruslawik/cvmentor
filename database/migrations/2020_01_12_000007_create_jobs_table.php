<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobsTable extends Migration
{
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');

            $table->longText('about');

            $table->string('univer_1');

            $table->string('univer_2');

            $table->string('univer_3');

            $table->string('univer_4')->nullable();

            $table->string('univer_5')->nullable();

            $table->string('sub_job_1');

            $table->string('sub_job_2');

            $table->string('sub_job_3');

            $table->string('sub_job_4');

            $table->string('sub_job_5');

            $table->timestamps();

            $table->softDeletes();
        });
    }
}
