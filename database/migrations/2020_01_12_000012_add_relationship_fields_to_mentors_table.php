<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToMentorsTable extends Migration
{
    public function up()
    {
        Schema::table('mentors', function (Blueprint $table) {
            $table->unsignedInteger('user_id');

            $table->foreign('user_id', 'user_fk_859967')->references('id')->on('users');

            $table->unsignedInteger('location_id');

            $table->foreign('location_id', 'location_fk_860035')->references('id')->on('locations');
        });
    }
}
