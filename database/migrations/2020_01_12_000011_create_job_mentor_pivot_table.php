<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobMentorPivotTable extends Migration
{
    public function up()
    {
        Schema::create('job_mentor', function (Blueprint $table) {
            $table->unsignedInteger('mentor_id');

            $table->foreign('mentor_id', 'mentor_id_fk_860036')->references('id')->on('mentors')->onDelete('cascade');

            $table->unsignedInteger('job_id');

            $table->foreign('job_id', 'job_id_fk_860036')->references('id')->on('jobs')->onDelete('cascade');
        });
    }
}
