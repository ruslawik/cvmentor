<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMentorsTable extends Migration
{
    public function up()
    {
        Schema::create('mentors', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('rating')->nullable();

            $table->string('speciality');

            $table->string('job_place');

            $table->string('job_experience');

            $table->string('education');

            $table->integer('price');

            $table->string('contacts');

            $table->longText('about');

            $table->string('useful_1');

            $table->string('useful_2');

            $table->string('useful_3');

            $table->string('date_1')->nullable();

            $table->string('date_2')->nullable();

            $table->string('date_3')->nullable();

            $table->string('date_4')->nullable();

            $table->string('date_5')->nullable();

            $table->integer('age');

            $table->boolean('online')->default(0)->nullable();

            $table->boolean('offline')->default(0)->nullable();

            $table->timestamps();

            $table->softDeletes();
        });
    }
}
