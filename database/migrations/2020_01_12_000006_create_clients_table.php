<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');

            $table->string('location');

            $table->string('education');

            $table->string('age');

            $table->string('phone');

            $table->integer('money');

            $table->string('selected_job');

            $table->string('certificates');

            $table->string('interests');

            $table->string('volunteer');

            $table->string('awards');

            $table->string('kabiletter');

            $table->string('weekness');

            $table->string('strongness');

            $table->longText('additional_info')->nullable();

            $table->timestamps();

            $table->softDeletes();
        });
    }
}
