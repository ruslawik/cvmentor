<form action="/search" method="GET">
@csrf
<div class="searching2">
    <div class="signIn__img"><img class="img-fluid" src="img/second/secondPage-2.jpg" alt=""></div>
    <div class="container big-container">
        <p class="results">Результаты поиска по запросу: "{{$query}}"</p>
        <div class="searching__inner">
            <input value="{{$query}}" name="query" type="text" class="inner__left col-10" placeholder="введите название специальности">
            <button class="inner__right" type="submit">искать</button>
        </div>
    </div>
</div>
