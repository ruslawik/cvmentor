@extends('front.layout.layout')
{{--Страница результатов поиска--}}

@section('content')
    
    {{--поиск --}}
    @include('front.search_result._include.search')

    {{--костыль версталы--}}
    <div class="spacer"></div>
    
    {{--фильтр --}}
    @include('front.search_result._include.filtres')
    
    {{--результаты поиска --}}
    @include('front.search_result._include.result_list')
    

@endsection

@section('js')
	<script>
	$(document).ready(function () {
        $("#cities").select2("val", "{{$city}}");
        $("#job_select").select2("val", "{{$job}}");
    });
    </script>
@endsection
