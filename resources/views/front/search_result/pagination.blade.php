<nav style="background:#fffeff !important;" class="my__pagination" aria-label="Page navigation example">
                <ul class="pagination pagination-sm">
                    <li class="page-item" style="background:#fffeff !important;">
                        <a style="background:#fffeff !important;" class="page-link" href="{{$paginator->previousPageUrl()}}" aria-label="Previous">
                            <span aria-hidden="true"><img src="img/slider/left-arrow.png" alt=""></span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </li>

    
                    <li class="page-item" style="background:#fffeff !important;">
                        <a style="background:#fffeff !important;" class="page-link" href="{{$paginator->nextPageUrl()}}" aria-label="Next">
                            <span aria-hidden="true"><img src="img/slider/right-arrow.png" alt=""></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </li>
                </ul>
</nav>