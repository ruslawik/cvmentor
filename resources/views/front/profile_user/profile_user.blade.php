@extends('front.layout.layout')
{{--Страница профиля пользователя--}}

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css" rel="stylesheet" />
@endsection

@section('content')
    
    {{--верхний блок --}}
    @include('front.profile_user._include.header')
    
    {{--форма --}}
    @include('front.profile_user._include.form')
    
@endsection

@section('modal')
    
    <!-- Modal -->
    <div class="modal fade" id="addMoney" tabindex="-1" role="dialog" aria-labelledby="addMoney" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Пополнить</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    В разработке. Подключаемся к модулю платежей.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                    <button type="button" class="btn btn-primary" style="background: #254a9e;">Пополнить</button>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Modal -->
    <div class="modal fade" id="historyPayModal" tabindex="-1" role="dialog" aria-labelledby="historyPayModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">История пополнений</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" style="background: #254a9e;">Save changes</button>
                </div>
            </div>
        </div>
    </div>


@endsection
