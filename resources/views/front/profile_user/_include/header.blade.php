<div class="thirdPage__intro">
    <div class="thirdPage__bg"><img class="img-fluid" src="/img/second/secondPage-2.jpg" alt=""></div>
    <div class="container">
        <div class="row">
            <div class="thirdPage__left col-lg-5" align="center">
                <img class="thirdPage__img img-fluid" width=79% 
                @if(isset($client) && $client->photo)
                    src="{{$client->photo->getUrl()}}" alt="">
                @else
                    src="/img/third/third-intro.png" alt="">
                @endif
                <div class="thirdPage__edit">
                    <img class="" src="/img/third/third-edit.png" alt="">
                    <span style="cursor:pointer;" data-toggle="modal" data-target="#replenishModal">редактировать профиль</span>
                </div>
            </div>
            <div class="thirdPage__right col-lg-7 mt-5">
                <h1 class="thirdPage__title">{{$client->user->name}} {{$client->user->surname}}</h1>
                <div class="thirdPage__text">
                    <p class="inner__text">Страна, город: {{$client->location}}</p>
                    <p class="inner__text">Образование: {{$client->education}}</p>
                    <p class="inner__text">Возраст: {{$client->age}}</p>
                    <p class="inner__text">Телефон: {{$client->phone}}</p>
                    <p class="inner__text">E-mail: {{$client->user->email}}</p>
                </div>
                <div class="thirdPageBtn__btn_section">
                    <p class="thirdPage__price">Личный баланс - @if($client->money == NULL) 0 @else {{$client->money}} @endif тг</p>
                    <a class="btn thirdPageBtn__left"  data-toggle="modal" data-target="#addMoney" type="button"><span>пополнить</span></a>
                    <a class="btn thirdPageBtn__right"  data-toggle="modal" data-target="#historyPayModal"  type="button"><span>история пополнений</span></a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
    <div class="modal fade" id="replenishModal" tabindex="-1" role="dialog" aria-labelledby="replenishModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Редактировать профиль</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
            <form method="POST" action="/user/profile-update-post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="user_id">{{ trans('cruds.client.fields.user') }}</label>
                : {{$client->user->email}}<br>
                @if($errors->has('user_id'))
                    <span class="text-danger">{{ $errors->first('user_id') }}</span>
                @endif
            </div>
            <div class="form-group">
                <label class="required" for="photo">{{ trans('cruds.client.fields.photo') }}</label>
                <div class="needsclick dropzone {{ $errors->has('photo') ? 'is-invalid' : '' }}" id="photo-dropzone">
                </div>
                @if($errors->has('photo'))
                    <span class="text-danger">{{ $errors->first('photo') }}</span>
                @endif
                <span class="help-block">Ваше фото</span>
            </div>

            <div class="form-group">
                <label class="required" for="client_name">Имя</label>
                <input class="form-control {{ $errors->has('client_name') ? 'is-invalid' : '' }}" type="text" name="client_name" id="client_name" value="{{$client->user->name}}" required>
            </div>

            <div class="form-group">
                <label class="required" for="client_surname">Фамилия</label>
                <input class="form-control {{ $errors->has('client_surname') ? 'is-invalid' : '' }}" type="text" name="client_surname" id="client_surname" value="{{$client->user->surname}}" required>
            </div>

            <div class="form-group">
                <label class="required" for="location">{{ trans('cruds.client.fields.location') }}</label>
                <input class="form-control {{ $errors->has('location') ? 'is-invalid' : '' }}" placeHolder="Казахстан, Нур-Султан" type="text" name="location" id="location" value="{{ old('location', $client->location) }}" required>
                @if($errors->has('location'))
                    <span class="text-danger">{{ $errors->first('location') }}</span>
                @endif
                <span class="help-block">Где Вы проживаете?</span>
            </div>
            <div class="form-group">
                <label class="required" for="education">{{ trans('cruds.client.fields.education') }}</label>
                <input class="form-control {{ $errors->has('education') ? 'is-invalid' : '' }}" placeHolder="Средняя школа №3 г. Алматы, Университет КАЗГЮУ" type="text" name="education" id="education" value="{{ old('education', $client->education) }}" required>
                @if($errors->has('education'))
                    <span class="text-danger">{{ $errors->first('education') }}</span>
                @endif
                <span class="help-block">Школа, университет, послевузовское образование (впишите, что относится к Вам)</span>
            </div>
            <div class="form-group">
                <label class="required" for="age">{{ trans('cruds.client.fields.age') }}</label>
                <input class="form-control {{ $errors->has('age') ? 'is-invalid' : '' }}" type="text" name="age" id="age" value="{{ old('age', $client->age) }}" required>
                @if($errors->has('age'))
                    <span class="text-danger">{{ $errors->first('age') }}</span>
                @endif
                <span class="help-block">Сколько Вам лет?</span>
            </div>
            <div class="form-group">
                <label class="required" for="phone">{{ trans('cruds.client.fields.phone') }}</label>
                <input class="form-control {{ $errors->has('phone') ? 'is-invalid' : '' }}" type="text" name="phone" id="phone" value="{{ old('phone', $client->phone) }}" required>
                @if($errors->has('phone'))
                    <span class="text-danger">{{ $errors->first('phone') }}</span>
                @endif
                <span class="help-block">Телефон для связи</span>
            </div>
            <div class="form-group">
                <label class="required" for="selected_job">{{ trans('cruds.client.fields.selected_job') }}</label>
                <input class="form-control {{ $errors->has('selected_job') ? 'is-invalid' : '' }}" type="text" name="selected_job" id="selected_job" value="{{ old('selected_job', $client->selected_job) }}" required>
                @if($errors->has('selected_job'))
                    <span class="text-danger">{{ $errors->first('selected_job') }}</span>
                @endif
                <span class="help-block">Какую профессию Вы выбрали/планируете выбрать?</span>
            </div>
            <div class="form-group">
                <label class="required" for="certificates">{{ trans('cruds.client.fields.certificates') }}</label>
                <input class="form-control {{ $errors->has('certificates') ? 'is-invalid' : '' }}" type="text" name="certificates" id="certificates" value="{{ old('certificates', $client->certificates) }}" required>
                @if($errors->has('certificates'))
                    <span class="text-danger">{{ $errors->first('certificates') }}</span>
                @endif
                <span class="help-block">Имеются ли у Вас сертификаты? Введите названия</span>
            </div>
            <div class="form-group">
                <label class="required" for="interests">{{ trans('cruds.client.fields.interests') }}</label>
                <input class="form-control {{ $errors->has('interests') ? 'is-invalid' : '' }}" type="text" name="interests" id="interests" value="{{ old('interests', $client->interests) }}" required>
                @if($errors->has('interests'))
                    <span class="text-danger">{{ $errors->first('interests') }}</span>
                @endif
                <span class="help-block">Чем Вы увлекаетесь?</span>
            </div>
            <div class="form-group">
                <label class="required" for="volunteer">{{ trans('cruds.client.fields.volunteer') }}</label>
                <input class="form-control {{ $errors->has('volunteer') ? 'is-invalid' : '' }}" type="text" name="volunteer" id="volunteer" value="{{ old('volunteer', $client->volunteer) }}" required>
                @if($errors->has('volunteer'))
                    <span class="text-danger">{{ $errors->first('volunteer') }}</span>
                @endif
                <span class="help-block">В каких волонтерских программах участвовали?</span>
            </div>
            <div class="form-group">
                <label class="required" for="awards">{{ trans('cruds.client.fields.awards') }}</label>
                <input class="form-control {{ $errors->has('awards') ? 'is-invalid' : '' }}" type="text" name="awards" id="awards" value="{{ old('awards', $client->awards) }}" required>
                @if($errors->has('awards'))
                    <span class="text-danger">{{ $errors->first('awards') }}</span>
                @endif
                <span class="help-block">Какие награды Вы получали? (спорт, музыка, наука, любые другие)</span>
            </div>
            <div class="form-group">
                <label class="required" for="kabiletter">{{ trans('cruds.client.fields.kabiletter') }}</label>
                <input class="form-control {{ $errors->has('kabiletter') ? 'is-invalid' : '' }}" type="text" name="kabiletter" id="kabiletter" value="{{ old('kabiletter', $client->kabiletter) }}" required>
                @if($errors->has('kabiletter'))
                    <span class="text-danger">{{ $errors->first('kabiletter') }}</span>
                @endif
                <span class="help-block">Как Вы считаете, к каким сферам у Вас есть больше способностей?</span>
            </div>
            <div class="form-group">
                <label class="required" for="weekness">{{ trans('cruds.client.fields.weekness') }}</label>
                <input class="form-control {{ $errors->has('weekness') ? 'is-invalid' : '' }}" type="text" name="weekness" id="weekness" value="{{ old('weekness', $client->weekness) }}" required>
                @if($errors->has('weekness'))
                    <span class="text-danger">{{ $errors->first('weekness') }}</span>
                @endif
                <span class="help-block">Опишите свои слабые стороны</span>
            </div>
            <div class="form-group">
                <label class="required" for="strongness">{{ trans('cruds.client.fields.strongness') }}</label>
                <input class="form-control {{ $errors->has('strongness') ? 'is-invalid' : '' }}" type="text" name="strongness" id="strongness" value="{{ old('strongness', $client->strongness) }}" required>
                @if($errors->has('strongness'))
                    <span class="text-danger">{{ $errors->first('strongness') }}</span>
                @endif
                <span class="help-block">Опишите свои сильные стороны</span>
            </div>
            <div class="form-group">
                <label for="additional_info">{{ trans('cruds.client.fields.additional_info') }}</label>
                <textarea class="form-control ckeditor {{ $errors->has('additional_info') ? 'is-invalid' : '' }}" name="additional_info" id="additional_info">{!! old('additional_info', $client->additional_info) !!}</textarea>
                @if($errors->has('additional_info'))
                    <span class="text-danger">{{ $errors->first('additional_info') }}</span>
                @endif
                <span class="help-block">Дополнительная информация, которую Вы считаете нужной для ментора</span>
            </div>
        
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                    <input type="submit" class="btn btn-primary" style="background: #254a9e;" value="Сохранить">
                </div>
            </div>
            </form>
        </div>
    </div>

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>
    <script>
    Dropzone.options.photoDropzone = {
    url: '{{ route('admin.mentors.storeMedia') }}',
    maxFilesize: 2, // MB
    acceptedFiles: '.jpeg,.jpg,.png,.gif',
    maxFiles: 1,
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 2,
      width: 4096,
      height: 4096
    },
    success: function (file, response) {
      $('form').find('input[name="photo"]').remove()
      $('form').append('<input type="hidden" name="photo" value="' + response.name + '">')
    },
    removedfile: function (file) {
      file.previewElement.remove()
      if (file.status !== 'error') {
        $('form').find('input[name="photo"]').remove()
        this.options.maxFiles = this.options.maxFiles + 1
      }
    },
    init: function () {
@if(isset($client) && $client->photo)
      var file = {!! json_encode($client->photo) !!}
          this.options.addedfile.call(this, file)
      this.options.thumbnail.call(this, file, '{{ $client->photo->getUrl('thumb') }}')
      file.previewElement.classList.add('dz-complete')
      $('form').append('<input type="hidden" name="photo" value="' + file.file_name + '">')
      this.options.maxFiles = this.options.maxFiles - 1
@endif
    },
    error: function (file, response) {
        if ($.type(response) === 'string') {
            var message = response //dropzone sends it's own error messages in string
        } else {
            var message = response.errors.file
        }
        file.previewElement.classList.add('dz-error')
        _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
        _results = []
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            node = _ref[_i]
            _results.push(node.textContent = message)
        }

        return _results
    }
}
</script>

    <script>
    //jQuery("#replenishModal").modal("show");
</script>
@endsection


        