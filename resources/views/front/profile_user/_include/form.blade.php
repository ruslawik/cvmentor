<div class="profile">
    <div class="container">
        <h1 class="profile__title">Анкета</h1>
        <div class="row justify-content-center profile__inner">
            <div class="profile-inner__left col-lg-12">
                <p class="profile-inner__text">Выбранная профессия</p>
                <table class="table table-striped table-borderless">
                    <tr >
                        <td>{{$client->selected_job}}</td>
                    </tr>
                </table>
                <p class="profile-inner__text">Сертификаты</p>
                <table class="table table-striped table-borderless">
                    <tr>
                        <td>{{$client->certificates}}</td>
                    </tr>
                </table>
                <p class="profile-inner__text">Интересы</p>
                <table class="table table-striped table-borderless">
                    <tr>
                        <td>{{$client->interests}}</td>
                    </tr>
                </table>
                <p class="profile-inner__text">Наименее сильные стороны</p>
                <table class="table table-striped table-borderless">
                    <tr>
                        <td>{{$client->weekness}}</td>
                    </tr>
                </table>
                <p class="profile-inner__text">Волонтерство</p>
                <table class="table table-striped table-borderless">
                    <tr>
                        <td>{{$client->volunteer}}</td>
                    </tr>
                </table>
                <p class="profile-inner__text">Награды</p>
                <table class="table table-striped table-borderless">
                    <tr>
                        <td>{{$client->awards}}</td>
                    </tr>
                </table>
                <p class="profile-inner__text">Способности</p>
                <table class="table table-striped table-borderless">
                    <tr>
                        <td>{{$client->kabiletter}}</td>
                    </tr>
                </table>
                <p class="profile-inner__text">Сильные стороны</p>
                <table class="table table-striped table-borderless">
                    <tr>
                        <td>{{$client->strongness}}</td>
                    </tr>
                </table>
                <p class="profile-inner__text">Дополнительно</p>
                <table class="table table-striped table-borderless">
                    <tr>
                        <td>{{$client->additional_info}}</td>
                    </tr>
                </table>
        </div>
    </div>
</div>
