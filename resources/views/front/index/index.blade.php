@extends('front.layout.layout')
{{--Главная страница--}}

@section('content')

    {{--интро--}}
    @include('front.index._include.intro')
    
    {{--поиск--}}
    @include('front.index._include.search')
    
    {{--костыль от версталы--}}
    <div class="spacer"></div>
    
    {{--фильтры--}}
    @include('front.index._include.filtres')
    
    {{--слайдер--}}
    @include('front.index._include.slider')
    
    {{--менторы--}}
    @include('front.index._include.mentors')

@endsection
