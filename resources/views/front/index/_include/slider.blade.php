<div class="main__slider">
    <div class="container">
        <h1 class="slider__title">Сфера профессий</h1>
        <div id="my-carousel" class="carousel slide" data-ride="carousel">
            <?php
                $k = 0;
            ?>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <div class="row justify-content-center">
            @foreach ($jobs as $job)
                <?php
                    if($k % 3 == 0 && $k!=0){
                ?>
                    </div></div>
                    <div class="carousel-item">
                    <div class="row justify-content-center">
                <?php
                    }
                ?>
                    <div class="col-lg-3">
                            <div class="card card__bg1">
                                <div class="card__icon"><img class="img-fluid" src="{{$job->photo->getUrl()}}" alt=""></div>
                                <hr class="slider__line">
                                <h4 class="card__title">{{$job->name}}</h4>
                                <p class="card__text"><a href="/proffession/{{$job->id}}" style="text-decoration:none;color:inherit;">подробнее&nbsp;&nbsp;&nbsp;&nbsp;<img src="img/slider/arrow-1.png" alt=""></a></p>
                            </div>
                    </div>
                <?php
                    $k++;
                ?>
            @endforeach
        </div>
    </div>
            </div>
            <br><br><br>
            <ol class="carousel-indicators my-indicators">
                <li data-target="#my-carousel" data-slide-to="0" class="active"></li>
                <?php
                    for($i=1; $i<$k/3; $i++){
                ?>
                    <li data-target="#my-carousel" data-slide-to="{{$i}}"></li>
                <?php
                    }
                ?>
            </ol>
            <a href="#my-carousel" class="carousel-control-prev" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a href="#my-carousel" class="carousel-control-next" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    <div class="carousel__bg"><img class="img-fluid" src="img/slider-div.jpg" alt=""></div>
</div>
