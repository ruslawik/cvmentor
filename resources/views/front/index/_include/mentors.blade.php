<div class="mentors">
    <div class="container">
        <h1 class="mentors__title">Новые менторы</h1>
        <div class="mentors__main">
            <?php
                $k = 0;
            ?>
            <div class="row justify-content-center">
            @foreach ($mentors as $mentor)
                <?php
                    if($k % 4 == 0 && $k!=0){
                ?>
                    </div>
                    <div class="row justify-content-center">
                <?php
                    }
                ?>
                    <div class="mentors__item col-3 col-md-3 col-sm-6">
                    <a style="text-decoration:none;color:inherit;" href="/mentor_page/{{$mentor->id}}"><img class="mentors__img img-fluid" src="@if(isset($mentor->photo)) {{$mentor->photo->getUrl()}} @else /img/third/third-intro.png @endif" alt="" width=90%>
                    <div class="mentors__name">{{$mentor->user->name}} {{$mentor->user->surname}}</div>
                    <div class="mentors__position">{{$mentor->speciality}}</div></a>
                    <!--<div class="mentors__icon"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star-half-alt"></i></div>!-->
                </div>
                <?php
                    $k++;
                ?>
            @endforeach
            </div>

            @include('front.index.pagination', ['paginator' => $mentors])
        
        </div>
    </div>
</div>
