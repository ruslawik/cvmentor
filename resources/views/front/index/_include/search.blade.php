<form action="/search" method="GET">
@csrf
<div class="searching">
    <div class="container big-container">
        <div class="searching__inner">
            <input type="text" name="query" class="inner__left col-10" placeholder="введите название специальности">
            <button class="inner__right" type="submit">искать</button>
        </div>
    </div>
</div>
