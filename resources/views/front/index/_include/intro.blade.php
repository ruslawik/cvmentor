<header class="intro">
    <div class="container big-container">
        <div class="row">
            <div class="intro__left col-lg-5">
                <h1 class="intro__title">CVMENTOR.KZ</h1>
                <h4 class="intro__suptitle">твой личный наставник</h4>
                <a href="/search"><button class="btn__call" type="button"><i class="fas fa-search img-fluid"></i>найти ментора</button></a>
                <a href="/user-register"><button class="btn__work" type="button"><i class="fas fa-lightbulb img-fluid"></i>стать ментором</button></a>
            </div>
            <div class="intro__right col-lg-7">
                <img src="img/intro-icon.jpg" style="width: 90%;" class="img-fluid" alt="responsive image">
            </div>
        </div>
    </div>
    <div class="intro__unclear">
        <img class="left__img img-fluid" src="img/intro-left.png" alt="">
        <img class="down__img img-fluid" src="img/intro-div.jpg" alt="">
    </div>
</header>
