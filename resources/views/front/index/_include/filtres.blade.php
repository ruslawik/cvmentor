<div class="filters">
    <div class="container big-container">
        <div class="filters__inner">
            <div class="inner__item">
                <p class="item__text">город</p>
                <select class="filters__form select_2" name="cities">
                    <option value="0">Все</option>
                    @foreach($locations as $location)
                        <option value="{{$location->id}}">{{$location->name}}</option>
                    @endforeach
                </select>
            </div>
            <!--
            <div class="inner__item">
                <p class="item__text">рейтинг</p>
                <fieldset class="rating">
                    <input class="input_1" type="radio" id="star5" name="rating" value="5" /><label class = "full label_2" for="star5"></label>
                    <input class="input_1" type="radio" id="star4half" name="rating" value="4 and a half" /><label class="half label_2" for="star4half"></label>
                    <input class="input_1" type="radio" id="star4" name="rating" value="4" /><label class = "full label_2" for="star4"></label>
                    <input class="input_1" type="radio" id="star3half" name="rating" value="3 and a half" /><label class="half label_2" for="star3half"></label>
                    <input class="input_1" type="radio" id="star3" name="rating" value="3" /><label class = "full label_2" for="star3"></label>
                    <input class="input_1" type="radio" id="star2half" name="rating" value="2 and a half" /><label class="half label_2" for="star2half"></label>
                    <input class="input_1" type="radio" id="star2" name="rating" value="2" /><label class = "full label_2" for="star2"></label>
                    <input class="input_1" type="radio" id="star1half" name="rating" value="1 and a half" /><label class="half label_2" for="star1half"></label>
                    <input class="input_1" type="radio" id="star1" name="rating" value="1" /><label class = "full label_2" for="star1"></label>
                    <input class="input_1" type="radio" id="starhalf" name="rating" value="half" /><label class="half label_2" for="starhalf"></label>
                </fieldset>
            </div>!-->
            <div class="inner__item inner__item_range" >
                <p class="item__text">цена</p>
                <input type="range" name="price" id="price" min="1000" max="20000" step="1000" value="20000" data-orientation="horizontal">
                <p class="range_view"> <span id="price_view">20000</span>₸</p>
                <!--					<input class="filters__form" type="text" value="Нур-Султан">-->
            </div>
            <div class="inner__item">
                <p class="item__text">форма занятия</p>
                
                <div class="toggle_radio filters__form1">
                    <input type="radio" class="toggle_option" id="first_toggle" name="online" checked>
                    <input type="radio" class="toggle_option" id="second_toggle" name="offline">
                    <label id="label_checked_form_1" class="label_toogle label_toogle_active" for="first_toggle"><p>онлайн</p></label>
                    <label id="label_checked_form_2" class="label_toogle label_revert_toogle" for="second_toggle"><p>встреча</p></label>
                    <!--					    <div class="toggle_option_slider"></div>-->
                </div>
                
                <!-- <ul class="filters__form inner__item1">
					<li class="filters__link">
						<input class="input__left " type='radio' value='1' name='radio' id="online" checked/>
						<label for='online'>онлайн</label>
					</li>
					<li class="filters__link">
						<input class="input__right" type='radio' value='2' name='radio'  id="meeting" unchecked/>
						<label for='meeting'>встреча</label>
					</li>
				</ul> -->
            </div>
            <div class="inner__item">
                <p class="item__text">сферы профессий</p>
                <select class="filters__form select_2" name="job">
                    <option value="0">Все</option>
                    @foreach($proffessions as $proffession)
                        <option value="{{$proffession->id}}">{{$proffession->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div><img class="img-fluid" src="img/filters-div.jpg" alt=""></div>
</div>
</form>
