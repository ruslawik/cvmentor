@extends('front.layout.layout')
{{--Страница профиля ментора--}}

@section('css')
	<link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css" rel="stylesheet" />
@endsection

@section('content')

    {{--информация о менторе ( первый блок ) --}}
    @include('front.profile_mentor._include.about_mentor_main')
    
    {{--информация о менторе ( второй блок ) --}}
    @include('front.profile_mentor._include.about_mentor_second')
    
    {{--информация о менторе ( третий блок ) --}}
    @include('front.profile_mentor._include.mentor_helpful_list')
    
    {{--отзывы о менторе  --}}
    {{--@include('front.profile_mentor._include.reviews')--}}

@endsection

@section('modal')


    <div class="modal fade" id="show_contacts" tabindex="-1" role="dialog" aria-labelledby="show_contacts" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Контакты ментора</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                   {{--$mentor->contacts--}}
                    По всем вопросам связи с ментором свяжитесь по номеру <br>
                    +7 (747) 489 83 71 - Менеджер Асель
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Modal -->
    <div class="modal fade" id="replenishModal" tabindex="-1" role="dialog" aria-labelledby="replenishModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Редактировать профиль ментора</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
            <form method="POST" action="/mentor/profile-update-post" enctype="multipart/form-data">
            @csrf
              <div class="form-group">
                <label class="required" for="user_id">{{ trans('cruds.mentor.fields.user') }}</label>
                 : {{$mentor->user->email}}
                <br>
                @if($errors->has('user_id'))
                    <span class="text-danger">{{ $errors->first('user_id') }}</span>
                @endif
            </div>
            <div class="form-group">
                <label class="required" for="photo">{{ trans('cruds.mentor.fields.photo') }}</label>
                <div class="needsclick dropzone {{ $errors->has('photo') ? 'is-invalid' : '' }}" id="photo-dropzone">
                </div>
                @if($errors->has('photo'))
                    <span class="text-danger">{{ $errors->first('photo') }}</span>
                @endif
                <span class="help-block">Ваше фото для профиля</span>
            </div>

            <div class="form-group">
                <label class="required" for="mentor_name">Имя</label>
                <input class="form-control {{ $errors->has('mentor_name') ? 'is-invalid' : '' }}" type="text" name="mentor_name" id="mentor_name" value="{{$mentor->user->name}}" required>
            </div>

            <div class="form-group">
                <label class="required" for="mentor_surname">Фамилия</label>
                <input class="form-control {{ $errors->has('mentor_surname') ? 'is-invalid' : '' }}" type="text" name="mentor_surname" id="mentor_surname" value="{{$mentor->user->surname}}" required>
            </div>

            <div class="form-group">
                <label class="required" for="speciality">{{ trans('cruds.mentor.fields.speciality') }}</label>
                <input class="form-control {{ $errors->has('speciality') ? 'is-invalid' : '' }}" type="text" name="speciality" id="speciality" value="{{ old('speciality', $mentor->speciality) }}" required>
                @if($errors->has('speciality'))
                    <span class="text-danger">{{ $errors->first('speciality') }}</span>
                @endif
                <span class="help-block">Укажите узкую область, в которой Вы специализируетесь</span>
            </div>
            <div class="form-group">
                <label class="required" for="jobs">{{ trans('cruds.mentor.fields.job') }}</label>
                <select style="width:100% !important;" class="select2 {{ $errors->has('jobs') ? 'is-invalid' : '' }}" name="jobs[]" id="jobs" multiple required>
                    @foreach($jobs as $id => $job)
                        <option value="{{ $id }}" {{ (in_array($id, old('jobs', [])) || $mentor->jobs->contains($id)) ? 'selected' : '' }}>{{ $job }}</option>
                    @endforeach
                </select>
                @if($errors->has('jobs'))
                    <span class="text-danger">{{ $errors->first('jobs') }}</span>
                @endif
                <span class="help-block">Выберите из списка для более точного поиска</span>
            </div>
            <div class="form-group">
                <label class="required" for="job_place">{{ trans('cruds.mentor.fields.job_place') }}</label>
                <input class="form-control {{ $errors->has('job_place') ? 'is-invalid' : '' }}" type="text" name="job_place" id="job_place" value="{{ old('job_place', $mentor->job_place) }}" required>
                @if($errors->has('job_place'))
                    <span class="text-danger">{{ $errors->first('job_place') }}</span>
                @endif
                <span class="help-block">Можно указать несколько, через запятую</span>
            </div>
            <div class="form-group">
                <label class="required" for="job_experience">{{ trans('cruds.mentor.fields.job_experience') }}</label>
                <input class="form-control {{ $errors->has('job_experience') ? 'is-invalid' : '' }}" type="text" name="job_experience" id="job_experience" value="{{ old('job_experience', $mentor->job_experience) }}" required>
                @if($errors->has('job_experience'))
                    <span class="text-danger">{{ $errors->first('job_experience') }}</span>
                @endif
                <span class="help-block">Можно указать несколько, через запятую</span>
            </div>
            <div class="form-group">
                <label class="required" for="education">{{ trans('cruds.mentor.fields.education') }}</label>
                <input class="form-control {{ $errors->has('education') ? 'is-invalid' : '' }}" type="text" name="education" id="education" value="{{ old('education', $mentor->education) }}" required>
                @if($errors->has('education'))
                    <span class="text-danger">{{ $errors->first('education') }}</span>
                @endif
                <span class="help-block">Ваша ученая степень или учебное заведение. Можно указать несколько, через запятую</span>
            </div>
            <div class="form-group">
                <label class="required" for="price_for_hour">{{ trans('cruds.mentor.fields.price') }}</label>
                <input class="form-control {{ $errors->has('price') ? 'is-invalid' : '' }}" type="number" name="price" id="price_for_hour" value="{{ old('price', $mentor->price) }}" step="1" required>
                @if($errors->has('price'))
                    <span class="text-danger">{{ $errors->first('price') }}</span>
                @endif
                <span class="help-block">Цена указывается за час консультаций</span>
            </div>
            <div class="form-group">
                <label class="required" for="contacts">{{ trans('cruds.mentor.fields.contacts') }}</label>
                <input class="form-control {{ $errors->has('contacts') ? 'is-invalid' : '' }}" type="text" name="contacts" id="contacts" value="{{ old('contacts', $mentor->contacts) }}" required>
                @if($errors->has('contacts'))
                    <span class="text-danger">{{ $errors->first('contacts') }}</span>
                @endif
                <span class="help-block">Важно! Укажите контакты, по которым клиенты смогут найти Вас</span>
            </div>
            <div class="form-group">
                <label class="required" for="location_id">{{ trans('cruds.mentor.fields.location') }}</label>
                <select style="width:100% !important;" class="select2 {{ $errors->has('location') ? 'is-invalid' : '' }}" name="location_id" id="location_id" required>
                    @foreach($locations as $id => $location)
                        <option value="{{ $id }}" {{ ($mentor->location ? $mentor->location->id : old('location_id')) == $id ? 'selected' : '' }}>{{ $location }}</option>
                    @endforeach
                </select>
                @if($errors->has('location_id'))
                    <span class="text-danger">{{ $errors->first('location_id') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.mentor.fields.location_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="age">{{ trans('cruds.mentor.fields.age') }}</label>
                <input class="form-control {{ $errors->has('age') ? 'is-invalid' : '' }}" type="number" name="age" id="age" value="{{ old('age', $mentor->age) }}" step="1" required>
                @if($errors->has('age'))
                    <span class="text-danger">{{ $errors->first('age') }}</span>
                @endif
                <span class="help-block">Укажите Ваш возраст</span>
            </div>
            <div class="form-group">
                <label class="required" for="about">{{ trans('cruds.mentor.fields.about') }}</label>
                <textarea class="form-control {{ $errors->has('about') ? 'is-invalid' : '' }} ckeditor" name="about" id="about" required>{{ old('about', $mentor->about) }}</textarea>
                @if($errors->has('about'))
                    <span class="text-danger">{{ $errors->first('about') }}</span>
                @endif
                <span class="help-block">Заполните историю о Вашем жизненном пути, успехах и разочарованиях, о том, почему Вы стали ментором</span>
            </div>
            <div class="form-group">
                <label class="required" for="useful_1">{{ trans('cruds.mentor.fields.useful_1') }}</label>
                <input class="form-control {{ $errors->has('useful_1') ? 'is-invalid' : '' }}" type="text" name="useful_1" id="useful_1" value="{{ old('useful_1', $mentor->useful_1) }}" required>
                @if($errors->has('useful_1'))
                    <span class="text-danger">{{ $errors->first('useful_1') }}</span>
                @endif
                <span class="help-block">Краткое предложение 1</span>
            </div>
            <div class="form-group">
                <label class="required" for="useful_2">{{ trans('cruds.mentor.fields.useful_2') }}</label>
                <input class="form-control {{ $errors->has('useful_2') ? 'is-invalid' : '' }}" type="text" name="useful_2" id="useful_2" value="{{ old('useful_2', $mentor->useful_2) }}" required>
                @if($errors->has('useful_2'))
                    <span class="text-danger">{{ $errors->first('useful_2') }}</span>
                @endif
                <span class="help-block">Краткое предложение 2</span>
            </div>
            <div class="form-group">
                <label class="required" for="useful_3">{{ trans('cruds.mentor.fields.useful_3') }}</label>
                <input class="form-control {{ $errors->has('useful_3') ? 'is-invalid' : '' }}" type="text" name="useful_3" id="useful_3" value="{{ old('useful_3', $mentor->useful_3) }}" required>
                @if($errors->has('useful_3'))
                    <span class="text-danger">{{ $errors->first('useful_3') }}</span>
                @endif
                <span class="help-block">Краткое предложение 3</span>
            </div>
            <div class="form-group">
                <label for="date_1">{{ trans('cruds.mentor.fields.date_1') }}</label>
                <input class="form-control {{ $errors->has('date_1') ? 'is-invalid' : '' }}" type="text" name="date_1" id="date_1" value="{{ old('date_1', $mentor->date_1) }}">
                @if($errors->has('date_1'))
                    <span class="text-danger">{{ $errors->first('date_1') }}</span>
                @endif
                <span class="help-block">Дата и время через запятую. Можете указать "Каждую неделю в среду с 13:00 до 15:00"</span>
            </div>
            <div class="form-group">
                <label for="date_2">{{ trans('cruds.mentor.fields.date_2') }}</label>
                <input class="form-control {{ $errors->has('date_2') ? 'is-invalid' : '' }}" type="text" name="date_2" id="date_2" value="{{ old('date_2', $mentor->date_2) }}">
                @if($errors->has('date_2'))
                    <span class="text-danger">{{ $errors->first('date_2') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.mentor.fields.date_2_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="date_3">{{ trans('cruds.mentor.fields.date_3') }}</label>
                <input class="form-control {{ $errors->has('date_3') ? 'is-invalid' : '' }}" type="text" name="date_3" id="date_3" value="{{ old('date_3', $mentor->date_3) }}">
                @if($errors->has('date_3'))
                    <span class="text-danger">{{ $errors->first('date_3') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.mentor.fields.date_3_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="date_4">{{ trans('cruds.mentor.fields.date_4') }}</label>
                <input class="form-control {{ $errors->has('date_4') ? 'is-invalid' : '' }}" type="text" name="date_4" id="date_4" value="{{ old('date_4', $mentor->date_4) }}">
                @if($errors->has('date_4'))
                    <span class="text-danger">{{ $errors->first('date_4') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.mentor.fields.date_4_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="date_5">{{ trans('cruds.mentor.fields.date_5') }}</label>
                <input class="form-control {{ $errors->has('date_5') ? 'is-invalid' : '' }}" type="text" name="date_5" id="date_5" value="{{ old('date_5', $mentor->date_5) }}">
                @if($errors->has('date_5'))
                    <span class="text-danger">{{ $errors->first('date_5') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.mentor.fields.date_5_helper') }}</span>
            </div>
            <div class="form-group">
                <div class="form-check {{ $errors->has('online') ? 'is-invalid' : '' }}">
                    <input type="hidden" name="online" value="0">
                    <input class="form-check-input" type="checkbox" name="online" id="online" value="1" {{ $mentor->online || old('online', 0) === 1 ? 'checked' : '' }}>
                    <label class="form-check-label" for="online">{{ trans('cruds.mentor.fields.online') }}</label>
                </div>
                @if($errors->has('online'))
                    <span class="text-danger">{{ $errors->first('online') }}</span>
                @endif
                <span class="help-block">Готовы ли Вы проводить консультации онлайн?</span>
            </div>
            <div class="form-group">
                <div class="form-check {{ $errors->has('offline') ? 'is-invalid' : '' }}">
                    <input type="hidden" name="offline" value="0">
                    <input class="form-check-input" type="checkbox" name="offline" id="offline" value="1" {{ $mentor->offline || old('offline', 0) === 1 ? 'checked' : '' }}>
                    <label class="form-check-label" for="offline">{{ trans('cruds.mentor.fields.offline') }}</label>
                </div>
                @if($errors->has('offline'))
                    <span class="text-danger">{{ $errors->first('offline') }}</span>
                @endif
                <span class="help-block">Готовы ли Вы к реальным встречам с клиентами?</span>
            </div>
            <div class="form-group">
                <label for="mentor_lang">Язык консультаций</label>
                <input class="form-control {{ $errors->has('mentor_lang') ? 'is-invalid' : '' }}" type="text" name="mentor_lang" id="mentor_lang" value="{{ old('mentor_lang', $mentor->mentor_lang) }}">
                @if($errors->has('mentor_lang'))
                    <span class="text-danger">{{ $errors->first('mentor_lang') }}</span>
                @endif
                <span class="help-block">Язык, на котором Вам удобно проводить консультации</span>
            </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                    <input type="submit" class="btn btn-primary" style="background: #254a9e;" value="Сохранить">
                </div>
            </div>
        </form>
        </div>
    </div>
 @endsection

@section('scripts')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
	<script src="{{ asset('js/main.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/16.0.0/classic/ckeditor.js"></script>
	<script>
    Dropzone.options.photoDropzone = {
    url: '{{ route('admin.mentors.storeMedia') }}',
    maxFilesize: 2, // MB
    acceptedFiles: '.jpeg,.jpg,.png,.gif',
    maxFiles: 1,
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 2,
      width: 4096,
      height: 4096
    },
    success: function (file, response) {
      $('form').find('input[name="photo"]').remove()
      $('form').append('<input type="hidden" name="photo" value="' + response.name + '">')
    },
    removedfile: function (file) {
      file.previewElement.remove()
      if (file.status !== 'error') {
        $('form').find('input[name="photo"]').remove()
        this.options.maxFiles = this.options.maxFiles + 1
      }
    },
    init: function () {
@if(isset($mentor) && $mentor->photo)
      var file = {!! json_encode($mentor->photo) !!}
          this.options.addedfile.call(this, file)
      this.options.thumbnail.call(this, file, '{{ $mentor->photo->getUrl('thumb') }}')
      file.previewElement.classList.add('dz-complete')
      $('form').append('<input type="hidden" name="photo" value="' + file.file_name + '">')
      this.options.maxFiles = this.options.maxFiles - 1
@endif
    },
    error: function (file, response) {
        if ($.type(response) === 'string') {
            var message = response //dropzone sends it's own error messages in string
        } else {
            var message = response.errors.file
        }
        file.previewElement.classList.add('dz-error')
        _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
        _results = []
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            node = _ref[_i]
            _results.push(node.textContent = message)
        }

        return _results
    }
}
</script>


<script>
    $(document).ready(function () {
  function SimpleUploadAdapter(editor) {
    editor.plugins.get('FileRepository').createUploadAdapter = function(loader) {
      return {
        upload: function() {
          return loader.file
            .then(function (file) {
              return new Promise(function(resolve, reject) {
                // Init request
                var xhr = new XMLHttpRequest();
                xhr.open('POST', '/admin/clients/ckmedia', true);
                xhr.setRequestHeader('x-csrf-token', '{{ csrf_token() }}');
                xhr.setRequestHeader('Accept', 'application/json');
                xhr.responseType = 'json';

                // Init listeners
                var genericErrorText = `Couldn't upload file: ${ file.name }.`;
                xhr.addEventListener('error', function() { reject(genericErrorText) });
                xhr.addEventListener('abort', function() { reject() });
                xhr.addEventListener('load', function() {
                  var response = xhr.response;

                  if (!response || xhr.status !== 201) {
                    return reject(response && response.message ? `${genericErrorText}\n${xhr.status} ${response.message}` : `${genericErrorText}\n ${xhr.status} ${xhr.statusText}`);
                  }

                  $('form').append('<input type="hidden" name="ck-media[]" value="' + response.id + '">');

                  resolve({ default: response.url });
                });

                if (xhr.upload) {
                  xhr.upload.addEventListener('progress', function(e) {
                    if (e.lengthComputable) {
                      loader.uploadTotal = e.total;
                      loader.uploaded = e.loaded;
                    }
                  });
                }

                // Send request
                var data = new FormData();
                data.append('upload', file);
                data.append('crud_id', 3);
                xhr.send(data);
              });
            })
        }
      };
    }
  }

  var allEditors = document.querySelectorAll('.ckeditor');
  for (var i = 0; i < allEditors.length; ++i) {
    ClassicEditor.create(
      allEditors[i], {
        extraPlugins: [SimpleUploadAdapter]
      }
    );
  }
});
</script>

@endsection
