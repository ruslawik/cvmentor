@extends('front.layout.layout')
{{--Страница профиля ментора--}}

@section('content')

    {{--информация о менторе ( первый блок ) --}}
    @include('front.profile_mentor._include_public_profile.about_mentor_main')
    
    {{--информация о менторе ( второй блок ) --}}
    @include('front.profile_mentor._include_public_profile.about_mentor_second')
    
    {{--информация о менторе ( третий блок ) --}}
    @include('front.profile_mentor._include_public_profile.mentor_helpful_list')
    
    {{--отзывы о менторе  --}}
    {{--@include('front.profile_mentor._include_public_profile.reviews')--}}


    <div class="modal fade" id="show_contacts" tabindex="-1" role="dialog" aria-labelledby="show_contacts" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Контакты</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{--$mentor->contacts--}}
                    По всем вопросам связи с ментором свяжитесь по номеру <br>
                    +7 (747) 489 83 71 - Менеджер Асель
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>
    
@endsection



