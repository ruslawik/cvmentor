<div class="about">
    <div class="secondPage__1"><img class="img-fluid" src="/img/second/secondPage-1.jpg" alt=""></div>
    <div class="container">
        <div class="about__title">Обо мне</div>
        <div class="row">
                <div class="col-lg-12">
                    <p class="about__text"><b>График консультаций</b></p><br>
                    <table class="table table-striped table-borderless">
                        <tr>
                            <td><p class="inner__text">{{$mentor->date_1}}</p></td>
                        </tr>
                        <tr>
                            <td><p class="inner__text">{{$mentor->date_2}}</p></td>
                        </tr>
                        <tr>
                            <td><p class="inner__text">{{$mentor->date_3}}</p></td>
                        </tr>
                        <tr>
                            <td><p class="inner__text">{{$mentor->date_4}}</p></td>
                        </tr>
                         <tr>
                            <td><p class="inner__text">{{$mentor->date_5}}</p></td>
                        </tr>
                    </table>
                </div>
                <div class="col-lg-12">
                    <br>
                    <p class="about__text"><b>Основная информация</b></p><br>
                    {!!$mentor->about!!} 
                </div>
        </div>
    </div>
</div>
