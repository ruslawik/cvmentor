<div class="secondPage">
    <div class="secondPage__2"><img class="img-fluid" src="/img/second/secondPage-2.jpg" alt=""></div>
    <div class="container container-person" >
        <div class="row no-gutters">
            <div class="secondPage__left col-lg-6" align="center"><br><br>
                <img class="secondPage__img" width=70% 
                @if(isset($mentor) && $mentor->photo)
                    src="{{$mentor->photo->getUrl()}}" alt="">
                @else
                    src="/img/third/third-intro.png" alt="">
                @endif
                <!--
                <div class="secondPage__icon"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="far fa-star"></i></div>
                !-->
            </div>
            <div class="secondPage__right col-lg-6 mt-5">
                <h1 class="secondPage__title">{{$mentor->user->name}} {{$mentor->user->surname}}</h1>
                @if($mentor->approved == 0)<font color="red">Ваш профиль не подтвержден администратором и пока не отображается в поиске.</font><br><br>@endif
                @if (\Session::has('message'))
                        <font color="red">{{ \Session::get('message') }}</font><br>
                @endif
                <div class="secondPage__text">
                    <div class="thirdPage__edit">
                        <img class="" src="/img/third/third-edit.png" alt="">
                        <span style="cursor:pointer;" data-toggle="modal" data-target="#replenishModal">редактировать профиль</span>
                    </div><br>
                    <p class="inner__text">Страна, город: @if(isset($mentor->location->name)){{$mentor->location->name}}@endif</p>
                    <p class="inner__text">Специализация: {{$mentor->speciality}}</p>
                    <p class="inner__text">Место работы: {{$mentor->job_place}}</p>
                    <p class="inner__text">Опыт работы: {{$mentor->job_experience}}</p>
                    <p class="inner__text">Образование: {{$mentor->education}}</p>
                    <p class="inner__text">Язык консультаций: {{$mentor->mentor_lang}}</p>
                </div>
                <h4 class="secondPage__price">{{$mentor->price}} тенге в час</h4>
                @if(\Auth::check())
                    <button class="secondPage__btn" type="submit" data-toggle="modal" data-target="#show_contacts">cвязаться</button>
                @else
                    <a href="/auth"><button class="secondPage__btn" type="submit">cвязаться</button></a>
                @endif
            </div>
        </div>
    </div>
</div>

