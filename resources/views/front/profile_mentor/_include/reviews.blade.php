<div class="reviews">
    <div class="container">
        <h1 class="reviews__title">Отзывы</h1>
        <div class="reviews__inner">
            <div class="row">
                <div class="reviews__item">
                    <div class="col-lg-3 col-md-12 col-sm-12">
                        <img class="reviews__photo img-fluid " src="/img/second/reviews-1.png" alt="">
                    </div>
                    <div class="col-lg-9 col-md-12 col-sm-12">
                        <div class="reviews__content ">
                            <div class="reviews__suptitle">Эрнест Бачурин&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;26.11.2019</div>
                            <p class="reviews__text">У меня с партнерами инвестиционно-консалтинговый бизнес, планируем начать деятельность на рынке Москвы в сегменте, где работает ментор. Общение с ментором было доброжелательным и продуктивным с позиции подтверждения/опровержения наших с партнерами гипотез о рынке. Полученные советы уже скорректировали план наших действий. Большое Спасибо.</p>
                            <div class="review_icon">
                                <div>
                                    <img src="/img/second/thumbs-green.png">
                                </div>
                                <span> 13 </span>
                            </div>
                            <div class="review_icon review_icon_margin_left">
                                <div>
                                    <img src="/img/second/thumbs-red.png">
                                </div>
                                <span> 2 </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="reviews__item">
                    <div class="col-lg-3 col-md-12 col-sm-12">
                        <img class="reviews__photo img-fluid " src="/img/second/reviews-1.png" alt="">
                    </div>
                    <div class="col-lg-9 col-md-12 col-sm-12">
                        <div class="reviews__content ">
                            <div class="reviews__suptitle">Эрнест Бачурин&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;26.11.2019</div>
                            <p class="reviews__text">У меня с партнерами инвестиционно-консалтинговый бизнес, планируем начать деятельность на рынке Москвы в сегменте, где работает ментор. Общение с ментором было доброжелательным и продуктивным с позиции подтверждения/опровержения наших с партнерами гипотез о рынке. Полученные советы уже скорректировали план наших действий. Большое Спасибо.</p>
                            <div class="review_icon">
                                <div>
                                    <img src="/img/second/thumbs-green.png">
                                </div>
                                <span> 13 </span>
                            </div>
                            <div class="review_icon review_icon_margin_left">
                                <div>
                                    <img src="/img/second/thumbs-red.png">
                                </div>
                                <span> 2 </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="reviews__item">
                    <div class="col-lg-3 col-md-12 col-sm-12">
                        <img class="reviews__photo img-fluid " src="/img/second/reviews-1.png" alt="">
                    </div>
                    <div class="col-lg-9 col-md-12 col-sm-12">
                        <div class="reviews__content ">
                            <div class="reviews__suptitle">Эрнест Бачурин&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;26.11.2019</div>
                            <p class="reviews__text">У меня с партнерами инвестиционно-консалтинговый бизнес, планируем начать деятельность на рынке Москвы в сегменте, где работает ментор. Общение с ментором было доброжелательным и продуктивным с позиции подтверждения/опровержения наших с партнерами гипотез о рынке. Полученные советы уже скорректировали план наших действий. Большое Спасибо.</p>
                            <div class="review_icon">
                                <div>
                                    <img src="/img/second/thumbs-green.png">
                                </div>
                                <span> 13 </span>
                            </div>
                            <div class="review_icon review_icon_margin_left">
                                <div>
                                    <img src="/img/second/thumbs-red.png">
                                </div>
                                <span> 2 </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <nav class="my__pagination" aria-label="Page navigation example">
            <ul class="pagination pagination-sm">
                <li class="page-item">
                    <a class="page-link" href="#" aria-label="Previous">
                        <span aria-hidden="true"><img src="/img/slider/left-arrow.png" alt=""></span>
                        <span class="sr-only">Previous</span>
                    </a>
                </li>
                
                <li class="page-item"><a class="page-link mx-2" href="#">1</a></li>
                <li class="page-item"><a class="page-link mr-2" href="#">2</a></li>
                <li class="page-item"><a class="page-link mr-2" href="#">3</a></li>
                
                <li class="page-item">
                    <a class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true"><img src="/img/slider/right-arrow.png" alt=""></span>
                        <span class="sr-only">Next</span>
                    </a>
                </li>
            </ul>
        </nav>
    
    
    </div>
</div>
