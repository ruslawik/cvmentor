<div class="profit">
    <div class="container">
        <div class="profit__title">Чем я могу быть полезен как ментор</div>
        
        <div class="row profit__inner">
            <div class="col-lg-12">
                <div class="profit__inner_row">
                    <div class="profit__img1">
                        <span>1</span>
                    </div>
                    <p class="profit__text">{{$mentor->useful_1}}</p>
                </div>
            </div>
        </div>
        <div class="row profit__inner">
            <div class="col-lg-12">
                <div class="profit__inner_row profit__inner_row_left_margin">
                    <div class="profit__img1">
                        <span>2</span>
                    </div>
                    <p class="profit__text">{{$mentor->useful_2}}</p>
                </div>
            </div>
        </div>
        <div class="row profit__inner">
            <div class="col-lg-12">
                <div class="profit__inner_row">
                    <div class="profit__img1">
                        <span>3</span>
                    </div>
                    <p class="profit__text">{{$mentor->useful_3}}</p>
                </div>
            </div>
        </div>
    
    </div>
    <!--<div class="secondPage__3"><img class="img-fluid" src="/img/second/secondPage-2.jpg" alt=""></div>!-->
</div>
