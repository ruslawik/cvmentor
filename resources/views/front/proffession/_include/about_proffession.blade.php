<div class="seventh__tech">
    <div class="container">
        <div class="row">
            <div class="tech__item col-6">
                <div class="tech__inner">
                    <img src="/img/seventh/bg-1.png" alt="" class="tech__icon img-fluid">
                    <div class="tech__title">О специальности</div>
                </div>
                <p class="tech__text">
                    {!!$job[0]->about!!}
                </p>
            </div>
            <div class="tech__item1 col-6">
                <div class="tech__inner">
                    <img src="/img/seventh/bg-3.png" alt="" class="tech__icon img-fluid">
                    <div class="tech__title">Университеты</div>
                </div>
                <div class="tech__inner1">
                    <img src="/img/seventh/bg-4.png" alt="" class="tech__icon1 img-fluid">
                    <p class="tech__text">{{$job[0]->univer_1}}</p>
                </div>
                <div class="tech__inner1">
                    <img src="/img/seventh/bg-4.png" alt="" class="tech__icon1 img-fluid">
                    <p class="tech__text">{{$job[0]->univer_2}}</p>
                </div>
                <div class="tech__inner1">
                    <img src="/img/seventh/bg-4.png" alt="" class="tech__icon1 img-fluid">
                    <p class="tech__text">{{$job[0]->univer_3}}</p>
                </div>
                <div class="tech__inner1">
                    <img src="/img/seventh/bg-4.png" alt="" class="tech__icon1 img-fluid">
                    <p class="tech__text">{{$job[0]->univer_4}}</p>
                </div>
                <div class="tech__inner1">
                    <img src="/img/seventh/bg-4.png" alt="" class="tech__icon1 img-fluid">
                    <p class="tech__text">{{$job[0]->univer_5}}</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="tech__item col-6">
                <div class="tech__inner">
                    <img src="/img/seventh/bg-2.png" alt="" class="tech__icon img-fluid">
                    <div class="tech__title">Подотрасли</div>
                </div>
                <div class="tech__inner1">
                    <img src="/img/seventh/bg-5.png" alt="" class="tech__icon1 img-fluid">
                    <p class="tech__text">{{$job[0]->sub_job_1}}</p>
                </div>
                <div class="tech__inner1">
                    <img src="/img/seventh/bg-5.png" alt="" class="tech__icon1 img-fluid">
                    <p class="tech__text">{{$job[0]->sub_job_2}}</p>
                </div>
                <div class="tech__inner1">
                    <img src="/img/seventh/bg-5.png" alt="" class="tech__icon1 img-fluid">
                    <p class="tech__text">{{$job[0]->sub_job_3}}</p>
                </div>
                <div class="tech__inner1">
                    <img src="/img/seventh/bg-5.png" alt="" class="tech__icon1 img-fluid">
                    <p class="tech__text">{{$job[0]->sub_job_4}}</p>
                </div>
                <div class="tech__inner1">
                    <img src="/img/seventh/bg-5.png" alt="" class="tech__icon1 img-fluid">
                    <p class="tech__text">{{$job[0]->sub_job_5}}</p>
                </div>
            </div>
            <div class="tech__item1 col-6">
                <p class="profession">Подходит ли мне эта профессия?</p>
                <br>
                <p class="profession">Пройдите онлайн тест и узнайте прямо сейчас!</p>
                <a href="http://careervision.kz" target="_blank"><button class="apply__test">пройти тест</button></a>
            </div>
        </div>
    </div>
</div>
