<div class="mentors__tech">
    <div><img class="img-fluid" src="/img/second/secondPage-2.jpg" alt=""></div>
    <div class="container">
        <h1 class="mentors__h1">Менторы данной профессии</h1>
        @foreach($results as $result)
            @if($result->approved == 1)
            <div class="sixth__pageItem">
            <div class="row">
                <div class="sixth__pageInner col-2">
                    <img @if(isset($result) && $result->photo)
                    src="{{$result->photo->getUrl()}}" alt="" class="sixth__pageIcon img-fluid">
                @else
                    src="/img/third/third-intro.png" alt="" class="sixth__pageIcon img-fluid">
                @endif
                </div>
                <div class="sixth__pageInner col-10">
                    <h5 class="sixth__pageTitle">{{$result->user->name}} {{$result->user->surname}}, {{$result->speciality}}</h5>
                    <p class="sixth__pageText">Место работы: {{$result->job_place}}</p>
                    <p class="sixth__pageText">Опыт работы: {{$result->job_experience}}</p>
                    <p class="sixth__pageText">Образование: {{$result->education}}</p>
                    <div class="sixth__pageAwesome">
                        <p class="sixth__pageCity">@if(isset($result->location->name)){{$result->location->name}}@endif</p>
                        <fieldset class="rating">
                            <input class="input_1" type="radio" id="star5" name="rating" value="5" /><label class = "full label_2" for="star5"></label>
                            <input class="input_1" type="radio" id="star4half" name="rating" value="4 and a half" /><label class="half label_2" for="star4half"></label>
                            <input class="input_1" type="radio" id="star4" name="rating" value="4" /><label class = "full label_2" for="star4"></label>
                            <input class="input_1" type="radio" id="star3half" name="rating" value="3 and a half" /><label class="half label_2" for="star3half"></label>
                            <input class="input_1" type="radio" id="star3" name="rating" value="3" /><label class = "full label_2" for="star3"></label>
                            <input class="input_1" type="radio" id="star2half" name="rating" value="2 and a half" /><label class="half label_2" for="star2half"></label>
                            <input class="input_1" type="radio" id="star2" name="rating" value="2" /><label class = "full label_2" for="star2"></label>
                            <input class="input_1" type="radio" id="star1half" name="rating" value="1 and a half" /><label class="half label_2" for="star1half"></label>
                            <input class="input_1" type="radio" id="star1" name="rating" value="1" /><label class = "full label_2" for="star1"></label>
                            <input class="input_1" type="radio" id="starhalf" name="rating" value="half" /><label class="half label_2" for="starhalf"></label>
                        </fieldset>
                        <div class="sixth__pagePrice">{{$result->price}}₸ в час, встреча</div>
                        <a target="_blank" href="/mentor_page/{{$result->id}}"><button class="sixth__pageBtn align-self-end">посмотреть профиль&nbsp;<img src="/img/sixth/btn-arrow.png" alt=""></button></a>
                    </div>
                </div>
            </div>
        </div>
        @endif
        @endforeach
    </div>
</div>

