<div class="seventh__page">
    <img src="/img/second/secondPage-2.jpg" alt="" class="seventh__pageIcon1 img-fluid">
    <div class="container">
        <div class="seventh__inner">
            <img width=60px src="{{$job[0]->photo->getUrl()}}" style="-webkit-filter: invert(100%);filter: grayscale(1) invert(1);" alt="" class="inner__seventhIcon img-fluid mr-5">
            <h1 class="inner__seventhTitle">{{$job[0]->name}}</h1>
        </div>
    </div>
    <img src="/img/second/secondPage-1.jpg" alt="" class="seventh__pageIcon2 img-fluid">
</div>
