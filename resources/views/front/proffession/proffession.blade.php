@extends('front.layout.layout')
{{--Страница проффесии--}}

@section('content')

    {{--заголовок--}}
    @include('front.proffession._include.header')
    
    {{--о проффессии--}}
    @include('front.proffession._include.about_proffession')
    
    {{--о список менторов--}}
    @include('front.proffession._include.mentor_list')
    
@endsection
