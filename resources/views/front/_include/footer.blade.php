<footer class="footer">
    <div class="footer__div">
        @if(\Request::is('search') || \Request::is('mentor_page/*') || \Request::is('mentor/profile'))
            <img class="img-fluid" src="/img/footer-div2.png" alt="">
        @else
            <img class="img-fluid" src="/img/footer-div.jpg" alt="">
        @endif
    </div>
    <div class="footer__main">
        <div class="container">
            
            
            <div class="row">
                
                <div class="footer__navigation col-3">
                    <a href="/" class="footer__links">Главная</a><br>
                    <a href="/auth" class="footer__links">Войти</a><br>
                    <a href="/oferta" class="footer__links">Правила пользования</a><br>
                    <a data-toggle="modal" data-target="#show_contacts_footer" href="#" class="footer__links">Контакты</a><br>
                    <!--<a href="#" class="footer__links">Карта сайта</a><br>!-->
                </div>
                <div class="footer_mezhdu"></div>
                <div class="footer__socials">
                    <div class="socials__title">Мы в соц.сетях</div>
                    <div class="footer__icons">
                        <div class="facebook__icon"><i class="fab fa-facebook-square"></i></div>
                        <div class="instagram__icon"><i class="fab fa-instagram"></i></div>
                        <div class="vkontakte__icon"><i class="fab fa-vk"></i></div>
                    </div>
                    <div class="footer__pax">
                        <p class="digitalpax mr-3">Разработано<br> DigitalPax</p>
                        <img class="footer__img img-fluid" src="/img/footer-logo.png" alt="">
                    </div>
                </div>
            </div>
            <div class="footer-copyright text-center">© CareerVision
                <div class="footer__data">2018-2020</div>
                <div class="footer__rights">Все права защищены</div>
            </div>
        </div>
    </div>
</footer>

 <div class="modal fade" id="show_contacts_footer" tabindex="-1" role="dialog" aria-labelledby="show_contacts" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Контакты</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{--$mentor->contacts--}}
                    По всем вопросам свяжитесь по номеру <br>
                    +7 (747) 489 83 71 - Менеджер Асель
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>
