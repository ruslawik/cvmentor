<nav class="navbar navbar-expand-lg navbar-light">
    
    <a class="navbar-brand" href="/"><img class="img__logo img-fluid" src="/img/logo.png" alt=""></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="/">главная</a>
            </li>
            <li class="nav-item">
                @if(Auth::check())
                    <a class="nav-link" href="/auth">личный кабинет</a>
                @else
                    <a class="nav-link" href="/user-register">стать ментором</a>
                @endif
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="modal" data-target="#show_contacts_footer" href="#">контакты</a>
            </li>
            <li class="nav-item nav-item-login">
                @if(Auth::check())
                    <a class="nav-link" href="/front-logout">выйти</a>
                @else
                    <a class="nav-link" href="/auth">войти</a>
                @endif
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    рус
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">каз</a>
                    <a class="dropdown-item" href="#">eng</a>
                    <a class="dropdown-item" href="#">tur</a>
                </div>
            </li>
        </ul>
    </div>
    <div class="form__bg col-2">
        @if(Auth::check())
            <a href="/front-logout" class="btn button__bg" type="submit">выйти</a>
        @else
            <a href="/auth" class="btn button__bg" type="submit">войти</a>
        @endif
    </div>
</nav>
