
<script src="/js/jquery.min.js"></script>
<script src="/js/popper.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/select2.min.js" ></script>
<script src="/js/rangeslider.js"></script>
<script src="/js/front_main.js" ></script>
<script>
    
    $(document).ready(function () {
        
        // инициализация всех селектов с данным классом
        $('.select_2').select2();
        
        
        $('#label_checked_form_1').click(function () {
            
            if($('#label_checked_form_1').hasClass('label_toogle_active'))
            {
                $('#label_checked_form_1').removeClass('label_toogle_active');
                $('#first_toggle').prop('checked', false).removeAttr("checked");
                $('#second_toggle').prop('checked', true).attr("checked", "checked");
                $('#label_checked_form_2').addClass('label_revert_toogle_active');
                
            }
            else
            {
                $('#first_toggle').prop('checked', true).attr("checked", "checked");
                $('#label_checked_form_1').addClass('label_toogle_active');
                $('#label_checked_form_2').removeClass('label_toogle_active');
                $('#label_checked_form_2').removeClass('label_revert_toogle_active');
            }
            
        });
        
        $('#label_checked_form_2').click(function () {
            
            if($('#label_checked_form_2').hasClass('label_revert_toogle_active'))
            {
                $('#label_checked_form_2').removeClass('label_toogle_active');
                $('#second_toggle').prop('checked', false).removeAttr("checked");
                $('#first_toggle').prop('checked', true).attr("checked", "checked");
                
                $('#label_checked_form_1').addClass('label_toogle_active');
            }
            else
            {
                $('#second_toggle').prop('checked', true).attr("checked", "checked");
                $('#label_checked_form_2').addClass('label_revert_toogle_active');
                $('#label_checked_form_1').removeClass('label_toogle_active');
            }
            
        });
        
        
        
        $('#price').rangeslider({
            
            // Feature detection the default is `true`.
            // Set this to `false` if you want to use
            // the polyfill also in Browsers which support
            // the native <input type="range"> element.
            polyfill: false,
            
            // Default CSS classes
            rangeClass: 'rangeslider',
            disabledClass: 'rangeslider--disabled',
            horizontalClass: 'rangeslider--horizontal',
            verticalClass: 'rangeslider--vertical',
            fillClass: 'rangeslider__fill',
            handleClass: 'rangeslider__handle',
            
            // Callback function
            onInit: function() {},
            
            // Callback function
            onSlide: function(position, value) {
                $('#price_view').text(this.value);
            },
            
            // Callback function
            onSlideEnd: function(position, value) {}
        });
        
    });
</script>
@yield('js')
@yield('scripts')
