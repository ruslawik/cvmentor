<meta charset="UTF-8">
<link rel="stylesheet" href="/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<link href="/css/google_font.css" rel="stylesheet">
<link rel="stylesheet" href="/css/select2.min.css">
<link rel="stylesheet" href="/css/rangeslider.css">
<link rel="stylesheet" href="/css/front_main.css">
<title>CareerVision</title>
<script src="/js/fe04b4448c.js" crossorigin="anonymous"></script>
@yield('css')
