@extends('front.layout.layout')
{{--Страница регистрации и авторизации --}}

@section('content')
    
    <div class="sign__in">
		<div class="signIn__img"><img class="img-fluid" src="img/second/secondPage-2.jpg" alt=""></div>
		<div class="container">
			<div class="row">
				<div class="signIn__left col-lg-6">
					<img class="signInLeft__img img-fluid" src="img/image.png" alt="">
				</div>
				<div class="signIn__right col-lg-6">
					<div class="right__items">
						<a href="/auth"><button class="signIn__btn3 mr-3" type="submit">войти</button></a>
						<a href="/user-register"><button class="signIn__btn4" type="submit">регистрация</button></a>
					</div>
				<form action="{{$action}}" method="POST">
					{{csrf_field()}}
					<div class="singIn__input">
					@if (\Session::has('message'))
                        {!! \Session::get('message') !!}
                        <br><br>
                    @endif
					<select class="filters__form form-control" name="user_type">
                        <option value="3">Я ментор</option>
                        <option value="2">Я хочу найти ментора</option>
                    </select>
						<input type="e-mail" name="email" class="signInInput__texts" placeholder="e-mail">
						<input type="text" name="name" class="signInInput__texts" placeholder="имя">
						<input type="text" name="surname" class="signInInput__texts" placeholder="фамилия">
						<input type="password" name="password" class="signInInput__texts" placeholder="пароль">
						<input type="password" name="password2" class="signInInput__texts" placeholder="повторите пароль">
						<br><br>
						<p><input type="checkbox" checked disabled> Регистрируясь на сайте, Вы принимаете <a target="_blank" href="/oferta">условия договора</a></p>
						<button class="signInto1">зарегистрироваться</button>
					</div>
				</form>
				</div>
			</div>
		</div>
	</div>


@endsection

@section('css')
    <style>
        .select2{
            width: 100% !important;
        }
    </style>
@endsection
