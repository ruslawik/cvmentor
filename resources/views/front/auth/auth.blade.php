@extends('front.layout.layout')
{{--Страница регистрации и авторизации --}}

@section('content')
    
    {{--форма регистрации авторизаци --}}
    @include('front.auth._include.form')


@endsection

@section('css')
    <style>
        .select2{
            width: 100% !important;
        }
    </style>
@endsection
