<div class="sign__in">
    <div class="signIn__img"><img class="img-fluid" src="/img/second/secondPage-2.jpg" alt=""></div>
    <div class="container">
        
        <div class="row">
            <div class="col-sm-12 col-lg-6 signIn__left ">
                <img class="signInLeft__img img-fluid" src="img/image.png" alt="">
            </div>
            <div class="signIn__right col-lg-6">
                <div class="right__item">
                    <a href="/auth"><button class="signIn__btn1 mr-3" type="submit">войти</button></a>
                    <a href="/user-register"><button class="signIn__btn2" type="submit">регистрация</button></a>
                </div>
            <form action="{{$action}}" method="POST">
                {{csrf_field()}}
                <div class="singIn__input">
                    @if (\Session::has('message'))
                        {!! \Session::get('message') !!}
                    @endif
                    <input type="text" name="email" class="signInInput__text" placeholder="e-mail">
                    <input type="password" name="password" class="signInInput__text" placeholder="пароль">
                    <p class="forget__password">забыли пароль?</p>
                    <button class="signInto">выполнить вход</button>
                </div>
            </form>
            </div>
        
        </div>
    </div>
</div>
