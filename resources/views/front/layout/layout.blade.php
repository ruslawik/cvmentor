<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">

<head>
    {{--подключение шапки--}}
    @include('front._include.head')
</head>

<body>

    {{--подключение менюшки--}}
    @include('front._include.menu')

    {{--отображение контента--}}
    @yield('content')
    
    @yield('modal')
    
    {{--подключение футера--}}
    @include('front._include.footer')
    
    {{--подключение скриптов футера--}}
    @include('front._include.footer_script')


</body>

</html>
