<div class="m-3">
    @can('mentor_create')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-success" href="{{ route("admin.mentors.create") }}">
                    {{ trans('global.add') }} {{ trans('cruds.mentor.title_singular') }}
                </a>
            </div>
        </div>
    @endcan
    <div class="card">
        <div class="card-header">
            {{ trans('cruds.mentor.title_singular') }} {{ trans('global.list') }}
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class=" table table-bordered table-striped table-hover datatable datatable-Mentor">
                    <thead>
                        <tr>
                            <th width="10">

                            </th>
                            <th>
                                {{ trans('cruds.mentor.fields.id') }}
                            </th>
                            <th>
                                {{ trans('cruds.mentor.fields.user') }}
                            </th>
                            <th>
                                {{ trans('cruds.mentor.fields.photo') }}
                            </th>
                            <th>
                                {{ trans('cruds.mentor.fields.job') }}
                            </th>
                            <th>
                                {{ trans('cruds.mentor.fields.job_place') }}
                            </th>
                            <th>
                                {{ trans('cruds.mentor.fields.education') }}
                            </th>
                            <th>
                                {{ trans('cruds.mentor.fields.price') }}
                            </th>
                            <th>
                                {{ trans('cruds.mentor.fields.location') }}
                            </th>
                            <th>
                                {{ trans('cruds.mentor.fields.age') }}
                            </th>
                            <th>
                                {{ trans('cruds.mentor.fields.online') }}
                            </th>
                            <th>
                                {{ trans('cruds.mentor.fields.offline') }}
                            </th>
                            <th>
                                &nbsp;
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($mentors as $key => $mentor)
                            <tr data-entry-id="{{ $mentor->id }}">
                                <td>

                                </td>
                                <td>
                                    {{ $mentor->id ?? '' }}
                                </td>
                                <td>
                                    {{ $mentor->user->email ?? '' }}
                                </td>
                                <td>
                                    @if($mentor->photo)
                                        <a href="{{ $mentor->photo->getUrl() }}" target="_blank">
                                            <img src="{{ $mentor->photo->getUrl('thumb') }}" width="50px" height="50px">
                                        </a>
                                    @endif
                                </td>
                                <td>
                                    @foreach($mentor->jobs as $key => $item)
                                        <span class="badge badge-info">{{ $item->name }}</span>
                                    @endforeach
                                </td>
                                <td>
                                    {{ $mentor->job_place ?? '' }}
                                </td>
                                <td>
                                    {{ $mentor->education ?? '' }}
                                </td>
                                <td>
                                    {{ $mentor->price ?? '' }}
                                </td>
                                <td>
                                    {{ $mentor->location->name ?? '' }}
                                </td>
                                <td>
                                    {{ $mentor->age ?? '' }}
                                </td>
                                <td>
                                    <span style="display:none">{{ $mentor->online ?? '' }}</span>
                                    <input type="checkbox" disabled="disabled" {{ $mentor->online ? 'checked' : '' }}>
                                </td>
                                <td>
                                    <span style="display:none">{{ $mentor->offline ?? '' }}</span>
                                    <input type="checkbox" disabled="disabled" {{ $mentor->offline ? 'checked' : '' }}>
                                </td>
                                <td>
                                    @can('mentor_show')
                                        <a class="btn btn-xs btn-primary" href="{{ route('admin.mentors.show', $mentor->id) }}">
                                            {{ trans('global.view') }}
                                        </a>
                                    @endcan

                                    @can('mentor_edit')
                                        <a class="btn btn-xs btn-info" href="{{ route('admin.mentors.edit', $mentor->id) }}">
                                            {{ trans('global.edit') }}
                                        </a>
                                    @endcan

                                    @can('mentor_delete')
                                        <form action="{{ route('admin.mentors.destroy', $mentor->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                            <input type="hidden" name="_method" value="DELETE">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                        </form>
                                    @endcan

                                </td>

                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('mentor_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.mentors.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'desc' ]],
    pageLength: 25,
  });
  $('.datatable-Mentor:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection