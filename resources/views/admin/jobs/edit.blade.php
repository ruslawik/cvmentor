@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.job.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.jobs.update", [$job->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.job.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', $job->name) }}" required>
                @if($errors->has('name'))
                    <span class="text-danger">{{ $errors->first('name') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.job.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="photo">Иконка професии</label>
                <div class="needsclick dropzone {{ $errors->has('photo') ? 'is-invalid' : '' }}" id="photo-dropzone">
                </div>
                @if($errors->has('photo'))
                    <span class="text-danger">{{ $errors->first('photo') }}</span>
                @endif
                <span class="help-block">Иконка профессии</span>
            </div>
            <div class="form-group">
                <label for="about">{{ trans('cruds.job.fields.about') }}</label>
                <textarea class="form-control ckeditor {{ $errors->has('about') ? 'is-invalid' : '' }}" name="about" id="about">{!! old('about', $job->about) !!}</textarea>
                @if($errors->has('about'))
                    <span class="text-danger">{{ $errors->first('about') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.job.fields.about_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="univer_1">{{ trans('cruds.job.fields.univer_1') }}</label>
                <input class="form-control {{ $errors->has('univer_1') ? 'is-invalid' : '' }}" type="text" name="univer_1" id="univer_1" value="{{ old('univer_1', $job->univer_1) }}" required>
                @if($errors->has('univer_1'))
                    <span class="text-danger">{{ $errors->first('univer_1') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.job.fields.univer_1_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="univer_2">{{ trans('cruds.job.fields.univer_2') }}</label>
                <input class="form-control {{ $errors->has('univer_2') ? 'is-invalid' : '' }}" type="text" name="univer_2" id="univer_2" value="{{ old('univer_2', $job->univer_2) }}" required>
                @if($errors->has('univer_2'))
                    <span class="text-danger">{{ $errors->first('univer_2') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.job.fields.univer_2_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="univer_3">{{ trans('cruds.job.fields.univer_3') }}</label>
                <input class="form-control {{ $errors->has('univer_3') ? 'is-invalid' : '' }}" type="text" name="univer_3" id="univer_3" value="{{ old('univer_3', $job->univer_3) }}" required>
                @if($errors->has('univer_3'))
                    <span class="text-danger">{{ $errors->first('univer_3') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.job.fields.univer_3_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="univer_4">{{ trans('cruds.job.fields.univer_4') }}</label>
                <input class="form-control {{ $errors->has('univer_4') ? 'is-invalid' : '' }}" type="text" name="univer_4" id="univer_4" value="{{ old('univer_4', $job->univer_4) }}">
                @if($errors->has('univer_4'))
                    <span class="text-danger">{{ $errors->first('univer_4') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.job.fields.univer_4_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="univer_5">{{ trans('cruds.job.fields.univer_5') }}</label>
                <input class="form-control {{ $errors->has('univer_5') ? 'is-invalid' : '' }}" type="text" name="univer_5" id="univer_5" value="{{ old('univer_5', $job->univer_5) }}">
                @if($errors->has('univer_5'))
                    <span class="text-danger">{{ $errors->first('univer_5') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.job.fields.univer_5_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="sub_job_1">{{ trans('cruds.job.fields.sub_job_1') }}</label>
                <input class="form-control {{ $errors->has('sub_job_1') ? 'is-invalid' : '' }}" type="text" name="sub_job_1" id="sub_job_1" value="{{ old('sub_job_1', $job->sub_job_1) }}" required>
                @if($errors->has('sub_job_1'))
                    <span class="text-danger">{{ $errors->first('sub_job_1') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.job.fields.sub_job_1_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="sub_job_2">{{ trans('cruds.job.fields.sub_job_2') }}</label>
                <input class="form-control {{ $errors->has('sub_job_2') ? 'is-invalid' : '' }}" type="text" name="sub_job_2" id="sub_job_2" value="{{ old('sub_job_2', $job->sub_job_2) }}" required>
                @if($errors->has('sub_job_2'))
                    <span class="text-danger">{{ $errors->first('sub_job_2') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.job.fields.sub_job_2_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="sub_job_3">{{ trans('cruds.job.fields.sub_job_3') }}</label>
                <input class="form-control {{ $errors->has('sub_job_3') ? 'is-invalid' : '' }}" type="text" name="sub_job_3" id="sub_job_3" value="{{ old('sub_job_3', $job->sub_job_3) }}" required>
                @if($errors->has('sub_job_3'))
                    <span class="text-danger">{{ $errors->first('sub_job_3') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.job.fields.sub_job_3_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="sub_job_4">{{ trans('cruds.job.fields.sub_job_4') }}</label>
                <input class="form-control {{ $errors->has('sub_job_4') ? 'is-invalid' : '' }}" type="text" name="sub_job_4" id="sub_job_4" value="{{ old('sub_job_4', $job->sub_job_4) }}" required>
                @if($errors->has('sub_job_4'))
                    <span class="text-danger">{{ $errors->first('sub_job_4') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.job.fields.sub_job_4_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="sub_job_5">{{ trans('cruds.job.fields.sub_job_5') }}</label>
                <input class="form-control {{ $errors->has('sub_job_5') ? 'is-invalid' : '' }}" type="text" name="sub_job_5" id="sub_job_5" value="{{ old('sub_job_5', $job->sub_job_5) }}" required>
                @if($errors->has('sub_job_5'))
                    <span class="text-danger">{{ $errors->first('sub_job_5') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.job.fields.sub_job_5_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection

@section('scripts')
<script>
    Dropzone.options.photoDropzone = {
    url: '{{ route('admin.jobs.storeMedia') }}',
    maxFilesize: 2, // MB
    acceptedFiles: '.jpeg,.jpg,.png,.gif',
    maxFiles: 1,
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 2,
      width: 4096,
      height: 4096
    },
    success: function (file, response) {
      $('form').find('input[name="photo"]').remove()
      $('form').append('<input type="hidden" name="photo" value="' + response.name + '">')
    },
    removedfile: function (file) {
      file.previewElement.remove()
      if (file.status !== 'error') {
        $('form').find('input[name="photo"]').remove()
        this.options.maxFiles = this.options.maxFiles + 1
      }
    },
    init: function () {
@if(isset($job) && $job->photo)
      var file = {!! json_encode($job->photo) !!}
          this.options.addedfile.call(this, file)
      this.options.thumbnail.call(this, file, '{{ $job->photo->getUrl('thumb') }}')
      file.previewElement.classList.add('dz-complete')
      $('form').append('<input type="hidden" name="photo" value="' + file.file_name + '">')
      this.options.maxFiles = this.options.maxFiles - 1
@endif
    },
    error: function (file, response) {
        if ($.type(response) === 'string') {
            var message = response //dropzone sends it's own error messages in string
        } else {
            var message = response.errors.file
        }
        file.previewElement.classList.add('dz-error')
        _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
        _results = []
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            node = _ref[_i]
            _results.push(node.textContent = message)
        }

        return _results
    }
}
</script>


<script>
    $(document).ready(function () {
  function SimpleUploadAdapter(editor) {
    editor.plugins.get('FileRepository').createUploadAdapter = function(loader) {
      return {
        upload: function() {
          return loader.file
            .then(function (file) {
              return new Promise(function(resolve, reject) {
                // Init request
                var xhr = new XMLHttpRequest();
                xhr.open('POST', '/admin/jobs/ckmedia', true);
                xhr.setRequestHeader('x-csrf-token', window._token);
                xhr.setRequestHeader('Accept', 'application/json');
                xhr.responseType = 'json';

                // Init listeners
                var genericErrorText = `Couldn't upload file: ${ file.name }.`;
                xhr.addEventListener('error', function() { reject(genericErrorText) });
                xhr.addEventListener('abort', function() { reject() });
                xhr.addEventListener('load', function() {
                  var response = xhr.response;

                  if (!response || xhr.status !== 201) {
                    return reject(response && response.message ? `${genericErrorText}\n${xhr.status} ${response.message}` : `${genericErrorText}\n ${xhr.status} ${xhr.statusText}`);
                  }

                  $('form').append('<input type="hidden" name="ck-media[]" value="' + response.id + '">');

                  resolve({ default: response.url });
                });

                if (xhr.upload) {
                  xhr.upload.addEventListener('progress', function(e) {
                    if (e.lengthComputable) {
                      loader.uploadTotal = e.total;
                      loader.uploaded = e.loaded;
                    }
                  });
                }

                // Send request
                var data = new FormData();
                data.append('upload', file);
                data.append('crud_id', {{ $job->id ?? 0 }});
                xhr.send(data);
              });
            })
        }
      };
    }
  }

  var allEditors = document.querySelectorAll('.ckeditor');
  for (var i = 0; i < allEditors.length; ++i) {
    ClassicEditor.create(
      allEditors[i], {
        extraPlugins: [SimpleUploadAdapter]
      }
    );
  }
});
</script>

@endsection