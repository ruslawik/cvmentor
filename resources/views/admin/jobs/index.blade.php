@extends('layouts.admin')
@section('content')
@can('job_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.jobs.create") }}">
                {{ trans('global.add') }} {{ trans('cruds.job.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.job.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <table class=" table table-bordered table-striped table-hover ajaxTable datatable datatable-Job">
            <thead>
                <tr>
                    <th width="10">

                    </th>
                    <th>
                        {{ trans('cruds.job.fields.id') }}
                    </th>
                    <th>
                        Иконка
                    </th>
                    <th>
                        {{ trans('cruds.job.fields.name') }}
                    </th>
                    <th>
                        {{ trans('cruds.job.fields.univer_1') }}
                    </th>
                    <th>
                        {{ trans('cruds.job.fields.univer_2') }}
                    </th>
                    <th>
                        {{ trans('cruds.job.fields.univer_3') }}
                    </th>
                    <th>
                        {{ trans('cruds.job.fields.univer_4') }}
                    </th>
                    <th>
                        {{ trans('cruds.job.fields.univer_5') }}
                    </th>
                    <th>
                        {{ trans('cruds.job.fields.sub_job_1') }}
                    </th>
                    <th>
                        {{ trans('cruds.job.fields.sub_job_2') }}
                    </th>
                    <th>
                        {{ trans('cruds.job.fields.sub_job_3') }}
                    </th>
                    <th>
                        {{ trans('cruds.job.fields.sub_job_4') }}
                    </th>
                    <th>
                        {{ trans('cruds.job.fields.sub_job_5') }}
                    </th>
                    <th>
                        &nbsp;
                    </th>
                </tr>
            </thead>
        </table>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('job_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}';
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.jobs.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).data(), function (entry) {
          return entry.id
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  let dtOverrideGlobals = {
    buttons: dtButtons,
    processing: true,
    serverSide: true,
    retrieve: true,
    aaSorting: [],
    ajax: "{{ route('admin.jobs.index') }}",
    columns: [
      { data: 'placeholder', name: 'placeholder' },
{ data: 'id', name: 'id' },
{ data: 'photo', name: 'photo', sortable: false, searchable: false },
{ data: 'name', name: 'name' },
{ data: 'univer_1', name: 'univer_1' },
{ data: 'univer_2', name: 'univer_2' },
{ data: 'univer_3', name: 'univer_3' },
{ data: 'univer_4', name: 'univer_4' },
{ data: 'univer_5', name: 'univer_5' },
{ data: 'sub_job_1', name: 'sub_job_1' },
{ data: 'sub_job_2', name: 'sub_job_2' },
{ data: 'sub_job_3', name: 'sub_job_3' },
{ data: 'sub_job_4', name: 'sub_job_4' },
{ data: 'sub_job_5', name: 'sub_job_5' },
{ data: 'actions', name: '{{ trans('global.actions') }}' }
    ],
    order: [[ 1, 'desc' ]],
    pageLength: 25,
  };
  $('.datatable-Job').DataTable(dtOverrideGlobals);
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
});

</script>
@endsection