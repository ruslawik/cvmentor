@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.job.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.jobs.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.job.fields.id') }}
                        </th>
                        <td>
                            {{ $job->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.job.fields.name') }}
                        </th>
                        <td>
                            {{ $job->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.job.fields.about') }}
                        </th>
                        <td>
                            {!! $job->about !!}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.job.fields.univer_1') }}
                        </th>
                        <td>
                            {{ $job->univer_1 }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.job.fields.univer_2') }}
                        </th>
                        <td>
                            {{ $job->univer_2 }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.job.fields.univer_3') }}
                        </th>
                        <td>
                            {{ $job->univer_3 }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.job.fields.univer_4') }}
                        </th>
                        <td>
                            {{ $job->univer_4 }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.job.fields.univer_5') }}
                        </th>
                        <td>
                            {{ $job->univer_5 }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.job.fields.sub_job_1') }}
                        </th>
                        <td>
                            {{ $job->sub_job_1 }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.job.fields.sub_job_2') }}
                        </th>
                        <td>
                            {{ $job->sub_job_2 }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.job.fields.sub_job_3') }}
                        </th>
                        <td>
                            {{ $job->sub_job_3 }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.job.fields.sub_job_4') }}
                        </th>
                        <td>
                            {{ $job->sub_job_4 }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.job.fields.sub_job_5') }}
                        </th>
                        <td>
                            {{ $job->sub_job_5 }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.jobs.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        {{ trans('global.relatedData') }}
    </div>
    <ul class="nav nav-tabs" role="tablist" id="relationship-tabs">
        <li class="nav-item">
            <a class="nav-link" href="#job_mentors" role="tab" data-toggle="tab">
                {{ trans('cruds.mentor.title') }}
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane" role="tabpanel" id="job_mentors">
            @includeIf('admin.jobs.relationships.jobMentors', ['mentors' => $job->jobMentors])
        </div>
    </div>
</div>

@endsection