@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.client.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.clients.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="user_id">{{ trans('cruds.client.fields.user') }}</label>
                <select class="form-control select2 {{ $errors->has('user') ? 'is-invalid' : '' }}" name="user_id" id="user_id" required>
                    @foreach($users as $id => $user)
                        <option value="{{ $id }}" {{ old('user_id') == $id ? 'selected' : '' }}>{{ $user }}</option>
                    @endforeach
                </select>
                @if($errors->has('user_id'))
                    <span class="text-danger">{{ $errors->first('user_id') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.client.fields.user_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="photo">{{ trans('cruds.client.fields.photo') }}</label>
                <div class="needsclick dropzone {{ $errors->has('photo') ? 'is-invalid' : '' }}" id="photo-dropzone">
                </div>
                @if($errors->has('photo'))
                    <span class="text-danger">{{ $errors->first('photo') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.client.fields.photo_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="location">{{ trans('cruds.client.fields.location') }}</label>
                <input class="form-control {{ $errors->has('location') ? 'is-invalid' : '' }}" type="text" name="location" id="location" value="{{ old('location', 'Не указано') }}" required>
                @if($errors->has('location'))
                    <span class="text-danger">{{ $errors->first('location') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.client.fields.location_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="education">{{ trans('cruds.client.fields.education') }}</label>
                <input class="form-control {{ $errors->has('education') ? 'is-invalid' : '' }}" type="text" name="education" id="education" value="{{ old('education', 'Не указано') }}" required>
                @if($errors->has('education'))
                    <span class="text-danger">{{ $errors->first('education') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.client.fields.education_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="age">{{ trans('cruds.client.fields.age') }}</label>
                <input class="form-control {{ $errors->has('age') ? 'is-invalid' : '' }}" type="text" name="age" id="age" value="{{ old('age', 'Не указано') }}" required>
                @if($errors->has('age'))
                    <span class="text-danger">{{ $errors->first('age') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.client.fields.age_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="phone">{{ trans('cruds.client.fields.phone') }}</label>
                <input class="form-control {{ $errors->has('phone') ? 'is-invalid' : '' }}" type="text" name="phone" id="phone" value="{{ old('phone', 'Не указано') }}" required>
                @if($errors->has('phone'))
                    <span class="text-danger">{{ $errors->first('phone') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.client.fields.phone_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="money">{{ trans('cruds.client.fields.money') }}</label>
                <input class="form-control {{ $errors->has('money') ? 'is-invalid' : '' }}" type="number" name="money" id="money" value="{{ old('money') }}" step="1" required>
                @if($errors->has('money'))
                    <span class="text-danger">{{ $errors->first('money') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.client.fields.money_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="selected_job">{{ trans('cruds.client.fields.selected_job') }}</label>
                <input class="form-control {{ $errors->has('selected_job') ? 'is-invalid' : '' }}" type="text" name="selected_job" id="selected_job" value="{{ old('selected_job', 'Не указано') }}" required>
                @if($errors->has('selected_job'))
                    <span class="text-danger">{{ $errors->first('selected_job') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.client.fields.selected_job_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="certificates">{{ trans('cruds.client.fields.certificates') }}</label>
                <input class="form-control {{ $errors->has('certificates') ? 'is-invalid' : '' }}" type="text" name="certificates" id="certificates" value="{{ old('certificates', 'Не указано') }}" required>
                @if($errors->has('certificates'))
                    <span class="text-danger">{{ $errors->first('certificates') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.client.fields.certificates_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="interests">{{ trans('cruds.client.fields.interests') }}</label>
                <input class="form-control {{ $errors->has('interests') ? 'is-invalid' : '' }}" type="text" name="interests" id="interests" value="{{ old('interests', 'Не указано') }}" required>
                @if($errors->has('interests'))
                    <span class="text-danger">{{ $errors->first('interests') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.client.fields.interests_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="volunteer">{{ trans('cruds.client.fields.volunteer') }}</label>
                <input class="form-control {{ $errors->has('volunteer') ? 'is-invalid' : '' }}" type="text" name="volunteer" id="volunteer" value="{{ old('volunteer', 'Не указано') }}" required>
                @if($errors->has('volunteer'))
                    <span class="text-danger">{{ $errors->first('volunteer') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.client.fields.volunteer_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="awards">{{ trans('cruds.client.fields.awards') }}</label>
                <input class="form-control {{ $errors->has('awards') ? 'is-invalid' : '' }}" type="text" name="awards" id="awards" value="{{ old('awards', 'Не указано') }}" required>
                @if($errors->has('awards'))
                    <span class="text-danger">{{ $errors->first('awards') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.client.fields.awards_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="kabiletter">{{ trans('cruds.client.fields.kabiletter') }}</label>
                <input class="form-control {{ $errors->has('kabiletter') ? 'is-invalid' : '' }}" type="text" name="kabiletter" id="kabiletter" value="{{ old('kabiletter', 'Не указано') }}" required>
                @if($errors->has('kabiletter'))
                    <span class="text-danger">{{ $errors->first('kabiletter') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.client.fields.kabiletter_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="weekness">{{ trans('cruds.client.fields.weekness') }}</label>
                <input class="form-control {{ $errors->has('weekness') ? 'is-invalid' : '' }}" type="text" name="weekness" id="weekness" value="{{ old('weekness', 'Не указано') }}" required>
                @if($errors->has('weekness'))
                    <span class="text-danger">{{ $errors->first('weekness') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.client.fields.weekness_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="strongness">{{ trans('cruds.client.fields.strongness') }}</label>
                <input class="form-control {{ $errors->has('strongness') ? 'is-invalid' : '' }}" type="text" name="strongness" id="strongness" value="{{ old('strongness', 'Не указано') }}" required>
                @if($errors->has('strongness'))
                    <span class="text-danger">{{ $errors->first('strongness') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.client.fields.strongness_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="additional_info">{{ trans('cruds.client.fields.additional_info') }}</label>
                <textarea class="form-control ckeditor {{ $errors->has('additional_info') ? 'is-invalid' : '' }}" name="additional_info" id="additional_info">{!! old('additional_info') !!}</textarea>
                @if($errors->has('additional_info'))
                    <span class="text-danger">{{ $errors->first('additional_info') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.client.fields.additional_info_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection

@section('scripts')
<script>
    Dropzone.options.photoDropzone = {
    url: '{{ route('admin.clients.storeMedia') }}',
    maxFilesize: 2, // MB
    acceptedFiles: '.jpeg,.jpg,.png,.gif',
    maxFiles: 1,
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 2,
      width: 4096,
      height: 4096
    },
    success: function (file, response) {
      $('form').find('input[name="photo"]').remove()
      $('form').append('<input type="hidden" name="photo" value="' + response.name + '">')
    },
    removedfile: function (file) {
      file.previewElement.remove()
      if (file.status !== 'error') {
        $('form').find('input[name="photo"]').remove()
        this.options.maxFiles = this.options.maxFiles + 1
      }
    },
    init: function () {
@if(isset($client) && $client->photo)
      var file = {!! json_encode($client->photo) !!}
          this.options.addedfile.call(this, file)
      this.options.thumbnail.call(this, file, '{{ $client->photo->getUrl('thumb') }}')
      file.previewElement.classList.add('dz-complete')
      $('form').append('<input type="hidden" name="photo" value="' + file.file_name + '">')
      this.options.maxFiles = this.options.maxFiles - 1
@endif
    },
    error: function (file, response) {
        if ($.type(response) === 'string') {
            var message = response //dropzone sends it's own error messages in string
        } else {
            var message = response.errors.file
        }
        file.previewElement.classList.add('dz-error')
        _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
        _results = []
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            node = _ref[_i]
            _results.push(node.textContent = message)
        }

        return _results
    }
}
</script>
<script>
    $(document).ready(function () {
  function SimpleUploadAdapter(editor) {
    editor.plugins.get('FileRepository').createUploadAdapter = function(loader) {
      return {
        upload: function() {
          return loader.file
            .then(function (file) {
              return new Promise(function(resolve, reject) {
                // Init request
                var xhr = new XMLHttpRequest();
                xhr.open('POST', '/admin/clients/ckmedia', true);
                xhr.setRequestHeader('x-csrf-token', window._token);
                xhr.setRequestHeader('Accept', 'application/json');
                xhr.responseType = 'json';

                // Init listeners
                var genericErrorText = `Couldn't upload file: ${ file.name }.`;
                xhr.addEventListener('error', function() { reject(genericErrorText) });
                xhr.addEventListener('abort', function() { reject() });
                xhr.addEventListener('load', function() {
                  var response = xhr.response;

                  if (!response || xhr.status !== 201) {
                    return reject(response && response.message ? `${genericErrorText}\n${xhr.status} ${response.message}` : `${genericErrorText}\n ${xhr.status} ${xhr.statusText}`);
                  }

                  $('form').append('<input type="hidden" name="ck-media[]" value="' + response.id + '">');

                  resolve({ default: response.url });
                });

                if (xhr.upload) {
                  xhr.upload.addEventListener('progress', function(e) {
                    if (e.lengthComputable) {
                      loader.uploadTotal = e.total;
                      loader.uploaded = e.loaded;
                    }
                  });
                }

                // Send request
                var data = new FormData();
                data.append('upload', file);
                data.append('crud_id', {{ $client->id ?? 0 }});
                xhr.send(data);
              });
            })
        }
      };
    }
  }

  var allEditors = document.querySelectorAll('.ckeditor');
  for (var i = 0; i < allEditors.length; ++i) {
    ClassicEditor.create(
      allEditors[i], {
        extraPlugins: [SimpleUploadAdapter]
      }
    );
  }
});
</script>

@endsection