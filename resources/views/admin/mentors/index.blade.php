@extends('layouts.admin')
@section('content')
@can('mentor_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.mentors.create") }}">
                {{ trans('global.add') }} {{ trans('cruds.mentor.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.mentor.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <table class=" table table-bordered table-striped table-hover ajaxTable datatable datatable-Mentor">
            <thead>
                <tr>
                    <th width="10">

                    </th>
                    <th>
                        {{ trans('cruds.mentor.fields.id') }}
                    </th>
                    <th>
                        APPROVED
                    </th>
                    <th>
                        {{ trans('cruds.mentor.fields.user') }}
                    </th>
                    <th>
                        {{ trans('cruds.mentor.fields.photo') }}
                    </th>
                    <th>
                        {{ trans('cruds.mentor.fields.job') }}
                    </th>
                    <th>
                        {{ trans('cruds.mentor.fields.job_place') }}
                    </th>
                    <th>
                        {{ trans('cruds.mentor.fields.education') }}
                    </th>
                    <th>
                        {{ trans('cruds.mentor.fields.price') }}
                    </th>
                    <th>
                        {{ trans('cruds.mentor.fields.location') }}
                    </th>
                    <th>
                        {{ trans('cruds.mentor.fields.age') }}
                    </th>
                    <th>
                        &nbsp;
                    </th>
                </tr>
            </thead>
        </table>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('mentor_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}';
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.mentors.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).data(), function (entry) {
          return entry.id
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  let dtOverrideGlobals = {
    buttons: dtButtons,
    processing: true,
    serverSide: true,
    retrieve: true,
    aaSorting: [],
    ajax: "{{ route('admin.mentors.index') }}",
    columns: [
      { data: 'placeholder', name: 'placeholder' },
{ data: 'id', name: 'id' },
{ data: 'approved', name: 'approved' },
{ data: 'user_email', name: 'user.email' },
{ data: 'photo', name: 'photo', sortable: false, searchable: false },
{ data: 'job', name: 'jobs.name' },
{ data: 'job_place', name: 'job_place' },
{ data: 'education', name: 'education' },
{ data: 'price', name: 'price' },
{ data: 'location_name', name: 'location.name' },
{ data: 'age', name: 'age' },
{ data: 'actions', name: '{{ trans('global.actions') }}' }
    ],
    order: [[ 1, 'desc' ]],
    pageLength: 25,
  };
  $('.datatable-Mentor').DataTable(dtOverrideGlobals);
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
});

</script>
@endsection