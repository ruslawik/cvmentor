@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.mentor.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.mentors.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.mentor.fields.id') }}
                        </th>
                        <td>
                            {{ $mentor->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.mentor.fields.user') }}
                        </th>
                        <td>
                            {{ $mentor->user->email ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.mentor.fields.photo') }}
                        </th>
                        <td>
                            @if($mentor->photo)
                                <a href="{{ $mentor->photo->getUrl() }}" target="_blank">
                                    <img src="{{ $mentor->photo->getUrl('thumb') }}" width="50px" height="50px">
                                </a>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.mentor.fields.rating') }}
                        </th>
                        <td>
                            {{ $mentor->rating }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.mentor.fields.speciality') }}
                        </th>
                        <td>
                            {{ $mentor->speciality }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.mentor.fields.job') }}
                        </th>
                        <td>
                            @foreach($mentor->jobs as $key => $job)
                                <span class="label label-info">{{ $job->name }}</span>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.mentor.fields.job_place') }}
                        </th>
                        <td>
                            {{ $mentor->job_place }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.mentor.fields.job_experience') }}
                        </th>
                        <td>
                            {{ $mentor->job_experience }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.mentor.fields.education') }}
                        </th>
                        <td>
                            {{ $mentor->education }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.mentor.fields.price') }}
                        </th>
                        <td>
                            {{ $mentor->price }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.mentor.fields.contacts') }}
                        </th>
                        <td>
                            {{ $mentor->contacts }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.mentor.fields.location') }}
                        </th>
                        <td>
                            {{ $mentor->location->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.mentor.fields.age') }}
                        </th>
                        <td>
                            {{ $mentor->age }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.mentor.fields.about') }}
                        </th>
                        <td>
                            {{ $mentor->about }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.mentor.fields.useful_1') }}
                        </th>
                        <td>
                            {{ $mentor->useful_1 }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.mentor.fields.useful_2') }}
                        </th>
                        <td>
                            {{ $mentor->useful_2 }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.mentor.fields.useful_3') }}
                        </th>
                        <td>
                            {{ $mentor->useful_3 }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.mentor.fields.date_1') }}
                        </th>
                        <td>
                            {{ $mentor->date_1 }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.mentor.fields.date_2') }}
                        </th>
                        <td>
                            {{ $mentor->date_2 }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.mentor.fields.date_3') }}
                        </th>
                        <td>
                            {{ $mentor->date_3 }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.mentor.fields.date_4') }}
                        </th>
                        <td>
                            {{ $mentor->date_4 }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.mentor.fields.date_5') }}
                        </th>
                        <td>
                            {{ $mentor->date_5 }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.mentor.fields.online') }}
                        </th>
                        <td>
                            <input type="checkbox" disabled="disabled" {{ $mentor->online ? 'checked' : '' }}>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.mentor.fields.offline') }}
                        </th>
                        <td>
                            <input type="checkbox" disabled="disabled" {{ $mentor->offline ? 'checked' : '' }}>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.mentors.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection