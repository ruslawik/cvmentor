@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.mentor.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.mentors.update", [$mentor->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="approved">APPROVED?</label>
                <input class="form-control {{ $errors->has('approved') ? 'is-invalid' : '' }}" type="text" name="approved" id="approved" value="{{ old('approved', $mentor->approved) }}" required>
                @if($errors->has('approved'))
                    <span class="text-danger">{{ $errors->first('approved') }}</span>
                @endif
                <span class="help-block">Подтверждение ментора для выдачи в поиске</span>
            </div>
            <div class="form-group">
                <label class="required" for="user_id">{{ trans('cruds.mentor.fields.user') }}</label>
                <select class="form-control select2 {{ $errors->has('user') ? 'is-invalid' : '' }}" name="user_id" id="user_id" required>
                    @foreach($users as $id => $user)
                        <option value="{{ $id }}" {{ ($mentor->user ? $mentor->user->id : old('user_id')) == $id ? 'selected' : '' }}>{{ $user }}</option>
                    @endforeach
                </select>
                @if($errors->has('user_id'))
                    <span class="text-danger">{{ $errors->first('user_id') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.mentor.fields.user_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="photo">{{ trans('cruds.mentor.fields.photo') }}</label>
                <div class="needsclick dropzone {{ $errors->has('photo') ? 'is-invalid' : '' }}" id="photo-dropzone">
                </div>
                @if($errors->has('photo'))
                    <span class="text-danger">{{ $errors->first('photo') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.mentor.fields.photo_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="rating">{{ trans('cruds.mentor.fields.rating') }}</label>
                <input class="form-control {{ $errors->has('rating') ? 'is-invalid' : '' }}" type="number" name="rating" id="rating" value="{{ old('rating', $mentor->rating) }}" step="1">
                @if($errors->has('rating'))
                    <span class="text-danger">{{ $errors->first('rating') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.mentor.fields.rating_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="speciality">{{ trans('cruds.mentor.fields.speciality') }}</label>
                <input class="form-control {{ $errors->has('speciality') ? 'is-invalid' : '' }}" type="text" name="speciality" id="speciality" value="{{ old('speciality', $mentor->speciality) }}" required>
                @if($errors->has('speciality'))
                    <span class="text-danger">{{ $errors->first('speciality') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.mentor.fields.speciality_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="jobs">{{ trans('cruds.mentor.fields.job') }}</label>
                <div style="padding-bottom: 4px">
                    <span class="btn btn-info btn-xs select-all" style="border-radius: 0">{{ trans('global.select_all') }}</span>
                    <span class="btn btn-info btn-xs deselect-all" style="border-radius: 0">{{ trans('global.deselect_all') }}</span>
                </div>
                <select class="form-control select2 {{ $errors->has('jobs') ? 'is-invalid' : '' }}" name="jobs[]" id="jobs" multiple required>
                    @foreach($jobs as $id => $job)
                        <option value="{{ $id }}" {{ (in_array($id, old('jobs', [])) || $mentor->jobs->contains($id)) ? 'selected' : '' }}>{{ $job }}</option>
                    @endforeach
                </select>
                @if($errors->has('jobs'))
                    <span class="text-danger">{{ $errors->first('jobs') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.mentor.fields.job_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="job_place">{{ trans('cruds.mentor.fields.job_place') }}</label>
                <input class="form-control {{ $errors->has('job_place') ? 'is-invalid' : '' }}" type="text" name="job_place" id="job_place" value="{{ old('job_place', $mentor->job_place) }}" required>
                @if($errors->has('job_place'))
                    <span class="text-danger">{{ $errors->first('job_place') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.mentor.fields.job_place_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="job_experience">{{ trans('cruds.mentor.fields.job_experience') }}</label>
                <input class="form-control {{ $errors->has('job_experience') ? 'is-invalid' : '' }}" type="text" name="job_experience" id="job_experience" value="{{ old('job_experience', $mentor->job_experience) }}" required>
                @if($errors->has('job_experience'))
                    <span class="text-danger">{{ $errors->first('job_experience') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.mentor.fields.job_experience_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="education">{{ trans('cruds.mentor.fields.education') }}</label>
                <input class="form-control {{ $errors->has('education') ? 'is-invalid' : '' }}" type="text" name="education" id="education" value="{{ old('education', $mentor->education) }}" required>
                @if($errors->has('education'))
                    <span class="text-danger">{{ $errors->first('education') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.mentor.fields.education_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="price">{{ trans('cruds.mentor.fields.price') }}</label>
                <input class="form-control {{ $errors->has('price') ? 'is-invalid' : '' }}" type="number" name="price" id="price" value="{{ old('price', $mentor->price) }}" step="1" required>
                @if($errors->has('price'))
                    <span class="text-danger">{{ $errors->first('price') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.mentor.fields.price_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="contacts">{{ trans('cruds.mentor.fields.contacts') }}</label>
                <input class="form-control {{ $errors->has('contacts') ? 'is-invalid' : '' }}" type="text" name="contacts" id="contacts" value="{{ old('contacts', $mentor->contacts) }}" required>
                @if($errors->has('contacts'))
                    <span class="text-danger">{{ $errors->first('contacts') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.mentor.fields.contacts_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="location_id">{{ trans('cruds.mentor.fields.location') }}</label>
                <select class="form-control select2 {{ $errors->has('location') ? 'is-invalid' : '' }}" name="location_id" id="location_id" required>
                    @foreach($locations as $id => $location)
                        <option value="{{ $id }}" {{ ($mentor->location ? $mentor->location->id : old('location_id')) == $id ? 'selected' : '' }}>{{ $location }}</option>
                    @endforeach
                </select>
                @if($errors->has('location_id'))
                    <span class="text-danger">{{ $errors->first('location_id') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.mentor.fields.location_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="age">{{ trans('cruds.mentor.fields.age') }}</label>
                <input class="form-control {{ $errors->has('age') ? 'is-invalid' : '' }}" type="number" name="age" id="age" value="{{ old('age', $mentor->age) }}" step="1" required>
                @if($errors->has('age'))
                    <span class="text-danger">{{ $errors->first('age') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.mentor.fields.age_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="about">{{ trans('cruds.mentor.fields.about') }}</label>
                <textarea class="form-control {{ $errors->has('about') ? 'is-invalid' : '' }}" name="about" id="about" required>{{ old('about', $mentor->about) }}</textarea>
                @if($errors->has('about'))
                    <span class="text-danger">{{ $errors->first('about') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.mentor.fields.about_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="useful_1">{{ trans('cruds.mentor.fields.useful_1') }}</label>
                <input class="form-control {{ $errors->has('useful_1') ? 'is-invalid' : '' }}" type="text" name="useful_1" id="useful_1" value="{{ old('useful_1', $mentor->useful_1) }}" required>
                @if($errors->has('useful_1'))
                    <span class="text-danger">{{ $errors->first('useful_1') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.mentor.fields.useful_1_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="useful_2">{{ trans('cruds.mentor.fields.useful_2') }}</label>
                <input class="form-control {{ $errors->has('useful_2') ? 'is-invalid' : '' }}" type="text" name="useful_2" id="useful_2" value="{{ old('useful_2', $mentor->useful_2) }}" required>
                @if($errors->has('useful_2'))
                    <span class="text-danger">{{ $errors->first('useful_2') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.mentor.fields.useful_2_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="useful_3">{{ trans('cruds.mentor.fields.useful_3') }}</label>
                <input class="form-control {{ $errors->has('useful_3') ? 'is-invalid' : '' }}" type="text" name="useful_3" id="useful_3" value="{{ old('useful_3', $mentor->useful_3) }}" required>
                @if($errors->has('useful_3'))
                    <span class="text-danger">{{ $errors->first('useful_3') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.mentor.fields.useful_3_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="date_1">{{ trans('cruds.mentor.fields.date_1') }}</label>
                <input class="form-control {{ $errors->has('date_1') ? 'is-invalid' : '' }}" type="text" name="date_1" id="date_1" value="{{ old('date_1', $mentor->date_1) }}">
                @if($errors->has('date_1'))
                    <span class="text-danger">{{ $errors->first('date_1') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.mentor.fields.date_1_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="date_2">{{ trans('cruds.mentor.fields.date_2') }}</label>
                <input class="form-control {{ $errors->has('date_2') ? 'is-invalid' : '' }}" type="text" name="date_2" id="date_2" value="{{ old('date_2', $mentor->date_2) }}">
                @if($errors->has('date_2'))
                    <span class="text-danger">{{ $errors->first('date_2') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.mentor.fields.date_2_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="date_3">{{ trans('cruds.mentor.fields.date_3') }}</label>
                <input class="form-control {{ $errors->has('date_3') ? 'is-invalid' : '' }}" type="text" name="date_3" id="date_3" value="{{ old('date_3', $mentor->date_3) }}">
                @if($errors->has('date_3'))
                    <span class="text-danger">{{ $errors->first('date_3') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.mentor.fields.date_3_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="date_4">{{ trans('cruds.mentor.fields.date_4') }}</label>
                <input class="form-control {{ $errors->has('date_4') ? 'is-invalid' : '' }}" type="text" name="date_4" id="date_4" value="{{ old('date_4', $mentor->date_4) }}">
                @if($errors->has('date_4'))
                    <span class="text-danger">{{ $errors->first('date_4') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.mentor.fields.date_4_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="date_5">{{ trans('cruds.mentor.fields.date_5') }}</label>
                <input class="form-control {{ $errors->has('date_5') ? 'is-invalid' : '' }}" type="text" name="date_5" id="date_5" value="{{ old('date_5', $mentor->date_5) }}">
                @if($errors->has('date_5'))
                    <span class="text-danger">{{ $errors->first('date_5') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.mentor.fields.date_5_helper') }}</span>
            </div>
            <div class="form-group">
                <div class="form-check {{ $errors->has('online') ? 'is-invalid' : '' }}">
                    <input type="hidden" name="online" value="0">
                    <input class="form-check-input" type="checkbox" name="online" id="online" value="1" {{ $mentor->online || old('online', 0) === 1 ? 'checked' : '' }}>
                    <label class="form-check-label" for="online">{{ trans('cruds.mentor.fields.online') }}</label>
                </div>
                @if($errors->has('online'))
                    <span class="text-danger">{{ $errors->first('online') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.mentor.fields.online_helper') }}</span>
            </div>
            <div class="form-group">
                <div class="form-check {{ $errors->has('offline') ? 'is-invalid' : '' }}">
                    <input type="hidden" name="offline" value="0">
                    <input class="form-check-input" type="checkbox" name="offline" id="offline" value="1" {{ $mentor->offline || old('offline', 0) === 1 ? 'checked' : '' }}>
                    <label class="form-check-label" for="offline">{{ trans('cruds.mentor.fields.offline') }}</label>
                </div>
                @if($errors->has('offline'))
                    <span class="text-danger">{{ $errors->first('offline') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.mentor.fields.offline_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection

@section('scripts')
<script>
    Dropzone.options.photoDropzone = {
    url: '{{ route('admin.mentors.storeMedia') }}',
    maxFilesize: 2, // MB
    acceptedFiles: '.jpeg,.jpg,.png,.gif',
    maxFiles: 1,
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 2,
      width: 4096,
      height: 4096
    },
    success: function (file, response) {
      $('form').find('input[name="photo"]').remove()
      $('form').append('<input type="hidden" name="photo" value="' + response.name + '">')
    },
    removedfile: function (file) {
      file.previewElement.remove()
      if (file.status !== 'error') {
        $('form').find('input[name="photo"]').remove()
        this.options.maxFiles = this.options.maxFiles + 1
      }
    },
    init: function () {
@if(isset($mentor) && $mentor->photo)
      var file = {!! json_encode($mentor->photo) !!}
          this.options.addedfile.call(this, file)
      this.options.thumbnail.call(this, file, '{{ $mentor->photo->getUrl('thumb') }}')
      file.previewElement.classList.add('dz-complete')
      $('form').append('<input type="hidden" name="photo" value="' + file.file_name + '">')
      this.options.maxFiles = this.options.maxFiles - 1
@endif
    },
    error: function (file, response) {
        if ($.type(response) === 'string') {
            var message = response //dropzone sends it's own error messages in string
        } else {
            var message = response.errors.file
        }
        file.previewElement.classList.add('dz-error')
        _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
        _results = []
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            node = _ref[_i]
            _results.push(node.textContent = message)
        }

        return _results
    }
}
</script>
@endsection