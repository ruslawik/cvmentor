<?php

//ФРОНТ ЧАСТЬ
/** Главная страница **/
Route::get('/', 'FrontPageController@index');

/* Текст договора */
Route::get('/oferta', 'FrontPageController@oferta');
Route::get('/oferta_kz', 'FrontPageController@oferta_kz');

/** Маршрут на страницу авторизации и регистрации */
Route::get('/auth', 'FrontPageController@auth');
Route::get('/user-register', 'FrontPageController@register');

Route::post('/register_post', 'FrontPageController@POSTregister');
Route::post('/auth_post', 'FrontPageController@POSTauth');

Route::get('/front-logout', 'FrontPageController@logout');

Route::get('/proffession/{id}', 'FrontPageController@jobPage');

Route::get('/search', 'FrontPageController@searchPage');

Route::get('/mentor_page/{id}', 'FrontPageController@mentorPage');


Route::group(['middleware' => ['auth.mentor'], 'prefix' => 'mentor'], function () {

    Route::any("/profile", "Mentor\MentorPageController@getProfile");
    Route::post("/profile-update-post", "Mentor\MentorPageController@profileUpdatePost");

});

Route::group(['middleware' => ['auth.user'], 'prefix' => 'user'], function () {

    Route::any("/profile", "User\UserPageController@getProfile");
    Route::post("/profile-update-post", "User\UserPageController@profileUpdatePost");

});




//БЭК ЧАСТЬ
Route::get('/home', function () {
    if (session('status')) {
        return redirect()->route('admin.home')->with('status', session('status'));
    }

    return redirect()->route('admin.home');
});

Auth::routes();
// Admin

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    // Permissions
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    // Users
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'UsersController');

    // Mentors
    Route::delete('mentors/destroy', 'MentorsController@massDestroy')->name('mentors.massDestroy');
    Route::post('mentors/media', 'MentorsController@storeMedia')->name('mentors.storeMedia');
    Route::post('mentors/ckmedia', 'MentorsController@storeCKEditorImages')->name('mentors.storeCKEditorImages');
    Route::resource('mentors', 'MentorsController');

    // Clients
    Route::delete('clients/destroy', 'ClientsController@massDestroy')->name('clients.massDestroy');
    Route::post('clients/media', 'ClientsController@storeMedia')->name('clients.storeMedia');
    Route::post('clients/ckmedia', 'ClientsController@storeCKEditorImages')->name('clients.storeCKEditorImages');
    Route::resource('clients', 'ClientsController');

    // Jobs
    Route::delete('jobs/destroy', 'JobsController@massDestroy')->name('jobs.massDestroy');
    Route::post('jobs/media', 'JobsController@storeMedia')->name('jobs.storeMedia');
    Route::post('jobs/ckmedia', 'JobsController@storeCKEditorImages')->name('jobs.storeCKEditorImages');
    Route::resource('jobs', 'JobsController');

    // Locations
    Route::delete('locations/destroy', 'LocationsController@massDestroy')->name('locations.massDestroy');
    Route::resource('locations', 'LocationsController');

    Route::get('global-search', 'GlobalSearchController@search')->name('globalSearch');
});
